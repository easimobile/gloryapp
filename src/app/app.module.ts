import { AuthService } from '../providers/auth-service/auth-service';
import { RequestServiceProvider } from '../providers/request-service/request-service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { File } from '@ionic-native/file';

import { MyApp } from './app.component';
import { HomePageModule } from '../pages/home/home.module';
import { LoginPageModule } from '../pages/login/login.module';
import { DepositPageModule } from '../pages/deposit/deposit.module';
import { UserPageModule } from '../pages/user/user.module'
import { ConfigurationPageModule } from '../pages/configuration/configuration.module';
import { DispensePageModule } from '../pages/dispense/dispense.module';
import { SalesPageModule } from '../pages/sales/sales.module';
import { RecyclerPageModule } from '../pages/recycler/recycler.module';
import { ReportPageModule } from '../pages/report/report.module';
import { SessionProvider } from '../providers/session/session';
import { RefundPageModule } from '../pages/refund/refund.module';
import { Insomnia } from '@ionic-native/insomnia'
import { StatusCheckProvider } from '../providers/status-check/status-check';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { FaultyPageModule } from '../pages/faulty/faulty.module';


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    LoginPageModule,
    HomePageModule,
    UserPageModule,
    DataTablesModule,
    DepositPageModule,
    DispensePageModule,
    ConfigurationPageModule,
    SalesPageModule,
    RecyclerPageModule,
    ReportPageModule,
    RefundPageModule,
    FaultyPageModule,
    NgIdleKeepaliveModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    NativeStorage,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthService,
    RequestServiceProvider,
    SessionProvider,
    Insomnia,
    StatusCheckProvider,
    File
  ]
})
export class AppModule { }
