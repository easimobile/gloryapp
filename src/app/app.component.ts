import { Component, ViewChild } from '@angular/core';
import { Platform, AlertController, App, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { UserPage } from '../pages/user/user';
import { DispensePage } from '../pages/dispense/dispense';
import { ConfigurationPage } from '../pages/configuration/configuration';
import { DepositPage } from '../pages/deposit/deposit';
import { SalesPage } from '../pages/sales/sales';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { HttpModule } from '@angular/http';
import { Insomnia } from '@ionic-native/insomnia';
import { NativeStorage } from '@ionic-native/native-storage';
import { RequestServiceProvider } from '../providers/request-service/request-service';
import { AuthService } from '../providers/auth-service/auth-service';
import { StatusCheckProvider } from '../providers/status-check/status-check';

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) nav: Nav
    //rootPage:any = HomePage;
    rootPage: any = LoginPage;
    //rootPage:any = UserPage;
    //rootPage:any = DispensePage;
    //rootPage:any = ConfigurationPage;
    //rootPage:any = SalesPage;

    idleState = 'Not started.';
    timedOut = false;
    lastPing?: Date = null;
    logoutTimeSetting;
    public statusLoop;

    constructor(private statusCheck: StatusCheckProvider, private insomnia: Insomnia, private nativeStorage: NativeStorage, private auth: AuthService, public requestService: RequestServiceProvider, private idle: Idle, private keepalive: Keepalive, public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public app: App, public alertCtrl: AlertController) {
        platform.ready().then(() => {
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.statusLoop = setInterval(() => this.statusCheck.checkStatusLoop(), 1000);  //1 seconds

        this.nativeStorage.getItem('session_time').then(time => {
            this.logoutTimeSetting = Number(time.time) * 60; //minute * 60 seconds
        })

        setTimeout(() => {
            // sets an idle timeout in seconds
            idle.setIdle(this.logoutTimeSetting);
            // sets a timeout period of logoutTimeSetting seconds. after logoutTimeSetting + 5 seconds of inactivity, the user will be considered timed out.
            idle.setTimeout(5);
            // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
            idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
            idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
            idle.onTimeout.subscribe(() => {
                this.idleState = 'Timed out!';
                this.timedOut = true;
                clearInterval(this.statusLoop);
                this.statusCheck.dismissLoadingCustom();
                this.sessionExpiredLogout();
            });

            idle.onIdleStart.subscribe(() => {
                this.idleState = 'You\'ve gone idle!';
                console.log(this.idleState);
            });

            idle.onTimeoutWarning.subscribe((countdown) => {
                this.idleState = 'You will time out in ' + countdown + ' seconds!';
                console.log(this.idleState);
            });


            // sets the ping interval to 15 seconds
            keepalive.interval(15);
            keepalive.onPing.subscribe(() => this.lastPing = new Date());
            this.reset();
        }, 1000);


        platform.registerBackButtonAction(() => {
            let nav = app.getActiveNavs()[0];
            let activeView = nav.getActive();
            if (activeView.name === "HomePage") {
                if (nav.canGoBack()) { //Can we go back?
                    nav.pop();
                } else {
                    const alert = this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                            text: 'Cancel',
                            role: 'cancel',
                            handler: () => {
                                console.log('Application exit prevented!');
                            }
                        }, {
                            text: 'Close App',
                            handler: () => {
                                this.platform.exitApp(); // Close this application
                            }
                        }]
                    });
                    alert.present();
                }
            }
        });

    }

    reset() {
        this.idle.watch();
        this.idleState = 'Started.';
        console.log(this.idleState);
        this.timedOut = false;
    }

    sessionExpiredLogout() {
        this.requestService.requestMachineStatus().then(data => {
            var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].$["n:result"];
            if (statusCode == 0) {
                this.auth.logout().subscribe(succ => {
                    window.localStorage.removeItem('status');
                    this.insomnia.allowSleepAgain();
                    this.nativeStorage.remove('Login_Info');
                    this.nav.setRoot(LoginPage);
                    console.log('logout');
                });
            }
            else {
                this.auth.logout().subscribe(succ => {
                    window.localStorage.removeItem('status');
                    this.insomnia.allowSleepAgain();
                    this.requestService.resetOperation();
                    this.nativeStorage.remove('Login_Info');
                    this.nav.setRoot(LoginPage);
                    console.log('logout');
                });
            }
        })
    }
}
