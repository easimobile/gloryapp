import { Component } from '@angular/core';
import { NavController, IonicPage, AlertController } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { NativeStorage } from '@ionic-native/native-storage';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Insomnia } from '@ionic-native/insomnia';
import { StatusCheckProvider } from '../../providers/status-check/status-check';
import { MyApp } from '../../app/app.component';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public username;
  public id;
  public status = '';
  public status2 = ''
  public level;
  public v: any[] = [];
  public v1: any[] = [];
  public cashier_ui: any[] = [];
  public su_ui: any[] = [];
  public admin_ui: any[] = [];
  public statusLoop;
  public storeArray: any[] = [];
  public min: any[] = [];
  public max: any[] = [];
  public current_inventory: any[] = [];
  public check_status;
  public current_total_amount;
  public inventory_double_check = 0;

  constructor(public AppComponent: MyApp, private insomnia: Insomnia, private statusCheck: StatusCheckProvider, private auth: AuthService, private alertCtrl: AlertController, private nativeStorage: NativeStorage, private nav: NavController, public requestService: RequestServiceProvider) {

    this.nativeStorage.getItem('Login_Info').then(info => {
      this.username = info.name;
      this.id = info.ID;
      this.level = info.level;
    });
  }
  // ionViewDidLoad vs ionViewWillEnter, ionViewWillEnter will load after back button, but ionViewDidLoad wont
  ionViewDidLoad() {
    //this.statusLoop = setInterval(() => this.statusCheck.checkStatusLoop(), 1000);  //1 seconds
    setTimeout(() => {
      this.checkAccess();
    }, 300);

    //this.nativeStorage.remove('transactionDetails');
    //this.nativeStorage.remove('2018-07-03operationDetails');
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.checkInventory();
    }, 300);
  }

  public checkAccess() {
    this.su_ui = [];
    this.admin_ui = [];
    this.cashier_ui = [];
    if (this.id == null) {    //no session set, return to login page
      this.nav.setRoot('LoginPage');
    }

    else {
      if (this.level == "003") {      //Super user
        this.nativeStorage.getItem('level_control_su').then(getConfigInfo => {
          for (let i = 0; i < getConfigInfo.length; i++) {
            if (getConfigInfo[i].control != 0)
              this.su_ui.push({ id: getConfigInfo[i].id, icon_name: getConfigInfo[i].icon_name, name: getConfigInfo[i].name, control: getConfigInfo[i].control })
          }
        })
      }
      else if (this.level == "002") {  //admin
        this.nativeStorage.getItem('level_control_admin').then(getConfigInfo => {
          for (let i = 0; i < getConfigInfo.length; i++) {
            if (getConfigInfo[i].control != 0)
              this.admin_ui.push({ id: getConfigInfo[i].id, icon_name: getConfigInfo[i].icon_name, name: getConfigInfo[i].name, control: getConfigInfo[i].control })
          }
        })
      }
      else if (this.level == "001") {   //cashier
        this.nativeStorage.getItem('level_control_cashier').then(getConfigInfo => {
          for (let i = 0; i < getConfigInfo.length; i++) {
            if (getConfigInfo[i].control != 0)
              this.cashier_ui.push({ id: getConfigInfo[i].id, icon_name: getConfigInfo[i].icon_name, name: getConfigInfo[i].name, control: getConfigInfo[i].control })
          }
        })
      }
    }
  }

  triggerFunction(id) {
    switch (Number(id)) {
      case 0:
        this.toDepositPage();
        break;

      case 1:
        this.toSalesPage();
        break;

      case 2:
        this.toDispensePage();
        break;

      case 3:
        this.toRefundPage();
        break;

      case 4:
        this.toInventoryPage();
        break;

      case 5:
        this.toPushPage();
        break;

      case 6:
        this.toLockUnlockPage();
        break;

      case 7:
        this.toReportPage();
        break;

      case 8:
        this.toUserPage();
        break;

      case 9:
        this.toRecyclerPage();
        break;

      case 10:
        this.coinReturnOperation();
        break;

      case 11:
        this.toConfigurationPage();
        break;

      case 12:
        this.toFaultyPage();
        break;

      case 13:
        this.resetConfirmation();
        break;

      /*
      case 14:
        this.rebootConfirmation();
        break;
      */

      case 15:
        this.logoutConfirmation();
        break;
    }
  }

  public checkInventory() {
    let check_1 = 0;
    let check_2 = 0;
    let minPiece = 0;
    let maxPiece = 0;
    this.max = [];
    this.min = [];
    let total_invetory = 0;
    this.current_total_amount = 0;
    this.current_inventory = [];
    this.requestService.inventoryOperation(2)
      .then(data => {
        let body_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
        this.nativeStorage.getItem('configuration').then(getConfigInfo => {

          for (let j = 0; j < body_recycler.length; j++) {
            let denominationType_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
            let denominationPiece_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
            this.v[j] = ((denominationType_4) / 100).toFixed(2);
            this.v1[j] = denominationPiece_4;
            minPiece = getConfigInfo[j].min;
            maxPiece = getConfigInfo[j].max;
            total_invetory += this.v[j] * this.v1[j];

            if (Number(this.v1[j]) < Number(minPiece)) {
              this.min[j] = this.v[j]+ ', ';;
              check_1 += 1;
            }

            if (Number(this.v1[j]) > Number(maxPiece)) {
              this.max[j] = this.v[j] + ', ';
              check_2 += 1;
            }

            this.current_inventory.push({ amount: this.v[j], piece: this.v1[j] });
          }

          if (check_1 > 0) {
            this.status = "SGD " + this.min.sort(function(a, b){return a - b}).join('').slice(0,-2) + " below minimum pieces.";
          }

          if (check_2 > 0) {
            this.status2 = " SGD " + this.max.sort(function(a, b){return a - b}).join('').slice(0,-2) + " exceed maximum pieces.";
          }

          this.current_total_amount = (total_invetory).toFixed(2);
          this.check_status = setInterval(() => this.checkstatus(), 1000);  //1 seconds

        })
      });

  }

  public toDepositPage() {
    this.nav.push('DepositPage');
  }

  public toSalesPage() {
    this.nav.push('SalesPage');
  }

  public toRefundPage() {
    this.nav.push('RefundPage');
  }

  public resetConfirmation() {
    let alert = this.alertCtrl.create({
      title: 'Reset Machine',
      message: 'Are You Sure To Reset The Machine?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.resetOperation();
          }
        }
      ]
    });
    alert.present();
  }

  public resetOperation() {
    this.requestService.resetOperation();
  }

  public toLockUnlockPage() {
    this.nav.push('UnlockPage');
  }

  public toInventoryPage() {
    this.nav.push('InventoryPage');
  }

  public toDispensePage() {
    this.nav.push('DispensePage');
  }

  public toPushPage() {
    this.nav.push('PushPage');
  }

  public toUserPage() {
    this.nav.push('UserPage');
  }

  public toConfigurationPage() {
    this.nav.push('ConfigurationPage');
  }

  public toRecyclerPage() {
    this.nav.push('RecyclerPage');
  }

  public toReportPage() {
    this.nav.push('ReportPage');
  }


  public toFaultyPage() {
    this.nav.push('FaultyPage');
  }

  public coinReturnOperation() {
    this.requestService.coinReturnOperation();
  }

  public rebootConfirmation() {
    let alert = this.alertCtrl.create({
      title: 'Reboot Machine',
      message: 'Are You Sure To Reboot The Machine?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.rebootMachine();
          }
        }
      ]
    });
    alert.present();
  }

  public rebootMachine() {
    //this.requestService.rebootMachine();
  }

  public logoutConfirmation() {
    let alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Do You Really Want To Exit?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.AppComponent.sessionExpiredLogout();
          }
        }
      ]
    });
    alert.present();
  }

  public logoutOperation() {
    this.AppComponent.sessionExpiredLogout();
  }

  checkstatus() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine


      if ((dev_id_1 == 1000 && dev_id_2 == 1000)) {
        console.log('dv 1: ' + dev_id_1);
        console.log('dv 2: ' + dev_id_2);
        clearInterval(this.check_status);
        setTimeout(() => {
          this.compareInventory();
        }, 300);
      }
      else {
        console.log('11dv 1: ' + dev_id_1);
        console.log('11dv 2: ' + dev_id_2);
      }
    })
  }

  compareInventory() {
    console.log('here');
    this.inventory_double_check += this.inventory_double_check;
    let last_inventory_total = 0;
    let amount_diff;
    this.nativeStorage.getItem('last_inventory').then(last_inventory_data => {
      console.log(last_inventory_data);
      for (let i = 0; i < last_inventory_data.length; i++) {
        last_inventory_total += (last_inventory_data[i].amount * last_inventory_data[i].piece);
      }

      console.log('Last Total ' + last_inventory_total.toFixed(2));
      console.log('Current Total ' + this.current_total_amount);

      if (last_inventory_total.toFixed(2) != this.current_total_amount) {
        amount_diff = Number(this.current_total_amount) - Number(last_inventory_total.toFixed(2))
        let alert = this.alertCtrl.create({
          title: 'Inventory',
          message: 'Inventory is not tally. <br> Amount different: SGD ' + amount_diff.toFixed(2),
          enableBackdropDismiss: false,
          buttons: [
            {
              text: 'OK',
              role: 'cancel',
              handler: () => {
                this.faultyRecord(amount_diff);
                this.saveInventory();
              }
            }
          ]
        });
        alert.present();
      }
      else {
        console.log("tally");
        if (this.inventory_double_check == 0) {
          console.log("double check");
          setTimeout(() => {
            this.compareInventory();
          }, 3500);
          this.inventory_double_check = 1;
        }
        else
          this.saveInventory();

      }
    },
      error => console.error('Error retrieve item', error));
  }

  saveInventory() {
    this.nativeStorage.setItem('last_inventory', this.current_inventory).then(
      () => console.log('Stored item'),
      error => console.error('Error storing item', error)
    );
  }

  faultyRecord(record) {
    var today = new Date();
    var yy = today.getFullYear();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + (today.getMonth() + 1)).slice(-2);

    let currentIndex = 0;
    let dateTime = yy + '-' + mm + '-' + dd;
    let fileName = dateTime;
    let new_recordList: any[] = [];
    let amt_diff = 0;

    let recordList = [{ index: '0', amount: record.toFixed(2) }];
    //this.nativeStorage.remove(fileName+'_faulty_record');
    this.nativeStorage.getItem(fileName + '_faulty_record').then(
      data => {
        for (let i = 0; i < data.length; i++) {
          currentIndex = Number(data[i].index);
          amt_diff = data[i].amount;
          new_recordList.push({ index: data[i].index, amount: Number(amt_diff).toFixed(2) });
        }

        new_recordList.push({ index: (currentIndex+1), amount: record.toFixed(2) });

        this.nativeStorage.setItem(fileName + '_faulty_record', new_recordList).then(
          () => console.log('Stored item'),
          error => console.error('Error storing item', error)
        );

        console.log(data);
      },
      error => {
        this.nativeStorage.setItem(fileName + '_faulty_record', recordList).then(
          () => console.log('Stored item'),
          error => console.error('Error storing item', error)
        );
      }
    );


  }
}