import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DispensePage } from './dispense';

@NgModule({
  declarations: [
    DispensePage,
  ],
  imports: [
    IonicPageModule.forChild(DispensePage),
  ],
})
export class DispensePageModule {}
