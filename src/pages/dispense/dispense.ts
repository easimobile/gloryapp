import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { NativeStorage } from '@ionic-native/native-storage';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-dispense',
  templateUrl: 'dispense.html',
})
export class DispensePage {
  private isDisabled: boolean = true;
  public u: any[] = [];
  public u1: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public checkDisable;
  public storeArray: any = [];
  public checkAmount_Piece_recycler: any[] = [];
  public dispenseDetail: any = [];
  public dispenseFailed;
  public allowToWithdrawId: any = [];
  public allowToWithdrawPiece: any = [];
  public dispenseByAmountArray: any = [];
  public negativeNote: any[] = [];
  public u2: number = 0;
  public loading;
  public loadingIsDisplay = 0;
  public statusLoop;
  dispenseForm: FormGroup;
  type_recycler = { "pieceInput": "" };
  public current_inventory: any[] = [];
  public updateInventoryCall = 0;
  public total_dispense = 0;
  public setRootCall = 0;
  public global_abcde;
  public status = false;
  
  constructor(private nativeStorage: NativeStorage, private file: File, public loadingCtrl: LoadingController, private nav: NavController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
  }

  ngOnInit() {
    this.dispenseForm = new FormGroup({
      //pieceInput: new FormControl('', [Validators.required, Validators.pattern('^([1-9][0-9]{0,2}|1000)$')])
      pieceInput: new FormControl('')
    });
  }

  ionViewDidLoad() {
    this.checkInventory();
  }

  public checkInventory() {
    this.storeArray = [];
    this.dispenseDetail = [];
    this.checkAmount_Piece_recycler = [];
    this.u2 = 0;
    this.requestService.inventoryOperation(2)
      .then(data => {
        console.log(data);
        let type_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].$["n:type"]; //4(Recycler)
        let body_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];

        for (let j = 0; j < body_recycler.length; j++) {
          let denominationType_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
          let denominationPiece_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
          let denominationDevid_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:devid"];

          this.v[j] = ((denominationType_4) / 100).toFixed(2);
          this.v1[j] = denominationPiece_4;
          this.checkDisable = true;

          if (this.v1[j] != 0) {
            this.checkDisable = false;
            this.storeArray.push({ id: j, type: denominationType_4, devid: denominationDevid_4, current: this.v1[j] });
          }
          this.dispenseDetail.push({ id: j, value: '', disabled: this.checkDisable });
          this.checkAmount_Piece_recycler.push({ amount: this.v[j], piece: this.v1[j], id: String(j), isDisabled: this.checkDisable, devid: denominationDevid_4 });
        }
        this.checkAmount_Piece_recycler.sort(function (a, b) { return a.amount - b.amount; });
      });
  }


  public dispense() {
    (<HTMLInputElement>document.getElementById("submit-btn")).disabled = true;
    let failed = 0;
    for (let i = 0; i < this.storeArray.length; i++) {
      this.allowToWithdrawId = this.storeArray[i].id;
      this.allowToWithdrawPiece[i] = this.dispenseDetail[this.allowToWithdrawId].value;
      console.log(this.storeArray[i].current);

      if(this.allowToWithdrawPiece[i] % 1 != 0){
        failed = 1;
      }

      if (this.allowToWithdrawPiece[i] < 1) {
        this.allowToWithdrawPiece[i] = 0;
      }

      if (Number(this.storeArray[i].current) < Number(this.allowToWithdrawPiece[i])) {
        failed = 1;
      }
      this.storeArray[i].piece = this.allowToWithdrawPiece[i];
      this.total_dispense += (this.storeArray[i].piece * ((this.storeArray[i].type) / 100));
    }

    //check result(failed or success)

    console.log(this.storeArray);
    console.log(failed);

    if (failed == 1) {
      let alert = this.alertCtrl.create({
        title: 'Failed To Dispense',
        subTitle: 'Invalid amount detected.',     //prompt message
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            role: 'dismiss',
            handler: () => {
              (<HTMLInputElement>document.getElementById("submit-btn")).disabled = false;
            }
          }
        ]
      });
      alert.present();
    } else {
      console.log('Start Dispense')
      this.requestService.cashoutOperation(this.storeArray).then(data => {
        var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:CashoutResponse"]["0"].$["n:result"];
        let statusLoop = setInterval(() => {
          this.checkStatusLoop();
          if(this.status == true){
            console.log('status ok');
            (<HTMLInputElement>document.getElementById("submit-btn")).disabled = false;
            clearInterval(statusLoop);
            this.transactionSaveStorage(statusCode);
            this.updateLastestInventory();
          }
        }, 600);  //1 seconds
      });
    }
  }

  public checkStatusLoop() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];

      console.log(dev_id_1);
      console.log(dev_id_2);
      if ((dev_id_1 == 1000 && dev_id_2 == 1000)) {
        this.status = true;
      }
      else {
        this.status = false
      }
    })
  }

  transactionSaveStorage(statusCode) {
    console.log('save transaction');
    var today = new Date();
    var yy = today.getFullYear();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + (today.getMonth() + 1)).slice(-2);
    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();

    let currentIndex = 0;
    let dateTime = dd + '_' + mm + '_' + hh + '_' + min + '_' + ss;
    let fileName = dateTime;
    let storageFileName = yy + '-' + mm + '-' + dd;
    let date = dd + '/' + mm + '/' + yy;
    let time = hh + ':' + min + ':' + ss;
    let msg = '';
    if (Number(statusCode) == 0) {
      msg = 'Success';
    }
    else if (Number(statusCode) == 10) {
      msg = 'Change Shortage, No Amount Dispensed.'
    }
    else if (Number(statusCode) == 11) {
      msg = 'Device Error'
    }
    else
      msg = statusCode;

    //let test = [{index: 0, id: fileName, date: date, time: time, sales: this.amount.get_amount, insert_amount: this.amount.cash, change: this.amount.change}]
    //this.nativeStorage.setItem('transactionDetails', test);


    this.nativeStorage.getItem(storageFileName + 'operationDetails').then(data => {
      this.storeArray = [];
      let transactionDetails_length = Number(data.length);
      for (let i = 0; i < transactionDetails_length; i++) {

        this.storeArray.push({ index: i, operation: data[i].operation, date: data[i].date, time: data[i].time, sales: data[i].sales, deposit_details: data[i].deposit_details, total_insert_amount: data[i].total_insert_amount, change: data[i].change, status: data[i].status });
        currentIndex = i + 1;
      }
    }, err => {
      console.log('not existing');
    })

    setTimeout(() => {
      this.storeArray.push({ index: currentIndex, operation: "Dispense", date: date, time: time, sales: '', deposit_details: '', total_insert_amount: '', change: (this.total_dispense).toFixed(2), status: msg });
      this.nativeStorage.setItem(storageFileName + 'operationDetails', this.storeArray);
    }, 1500);


    this.file.checkDir(this.file.externalDataDirectory, 'dispense').
      then(_ => {
        console.log('Directory exists');
        this.file.writeFile(
          this.file.externalDataDirectory + '/dispense',
          fileName + '.txt',
          'Dispense Information\n' +
          '==================================\n' +
          'Status: ' + msg + '\n' +
          'Date: ' + date + '\n' +
          'Time: ' + time + '\n' +
          'Dispense Amount (SGD): ' + (this.total_dispense).toFixed(2) + '\n',
          { replace: false }).
          then(_ => console.log('Success')).
          catch(err => console.log('Failed'));
      }
      ).
      catch(
        err => {
          console.log('Directory doesn\'t exist');
          this.file.createDir(this.file.externalDataDirectory, 'dispense', false);
          this.file.writeFile(
            this.file.externalDataDirectory + '/dispense',
            fileName + '.txt',
            'Dispense Information\n' +
            '==================================\n' +
            'Date: ' + date + '\n' +
            'Time: ' + time + '\n' +
            'Dispense Amount (SGD): ' + (this.total_dispense).toFixed(2) + '\n' +
            'Status: ' + msg + '\n',
            { replace: false }).
            then(_ => console.log('Success')).
            catch(err => console.log('Failed'));
        }
      );
  }

  public resetBtnOnClick() {
    this.dispenseForm.reset();
  }

  updateLastestInventory() {
    this.updateInventoryCall = 1;
    this.loading = this.loadingCtrl.create({
      content: 'Updating Inventory...'
    });

    this.loading.present();

    this.current_inventory = [];

    var abc = setInterval(() => {
      this.testingb();
      if (this.global_abcde == true) {
        clearInterval(abc);
        this.testingc();
      }
    }, 500);

  }


  testingb() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      if (dev_id_1 == '1000' && dev_id_2 == '1000') {
        this.global_abcde = true;
      }
      else {
        this.global_abcde = false;
      }
    });
  }


  testingc() {
    console.log('testing c');
    var abcde = setInterval(() => {
      this.testingd();
      if (this.testingd() == true) {
        clearInterval(abcde);
        this.testinge();
      }
    }, 1500);

  }

  testingd() {
    var sts = true;
    this.requestService.inventoryOperation(2).then(sales => {
      let stts = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].$["n:result"];
      if (stts == '10') {
        sts = true;
      }
      else {
        sts = false;
      }
    });
    return sts;
  }

  testinge() {
    this.requestService.inventoryOperation(2).then(sales => {
      console.log('here');
      console.log(sales);
      let body_recycler = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
      for (let j = 0; j < body_recycler.length; j++) {
        let denominationType_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
        let denominationPiece_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
        this.v[j] = ((denominationType_4) / 100).toFixed(2);
        this.v1[j] = denominationPiece_4;

        this.current_inventory.push({ amount: this.v[j], piece: this.v1[j] });

        console.log('new current invent');
        console.log(this.current_inventory);

        setTimeout(() => {
          console.log('new current invent2');
          console.log(this.current_inventory);
          this.nativeStorage.setItem('last_inventory', this.current_inventory).then(
            () => {
              console.log('updated lastest inventory');
              if(this.setRootCall == 0){
                this.setRootCall = 1;
                this.nav.setRoot(HomePage);
              }
              this.updateInventoryCall = 0;
              this.loading.dismiss();
            },
            error => console.error('Error storing item', error)
          );
        }, 1400);
      }
    });
  }
}
