import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { AlertController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-sales',
  templateUrl: 'sales.html',
})
export class SalesPage {
  private isDisabled: boolean = true;
  salesForm: FormGroup;
  amount = { "get_amount": "", "cash": '', "change": '' };
  public status: Number = 3;  //default testing
  public code: Number = 3;    //default testing
  public denomation: Number;
  public amountCount: any[] = [];
  public u: any[] = [];
  public u1: any[] = [];
  public u2: number = 0;
  public loop;
  loading: Loading;
  public tableView;
  id = '';
  public storeArray: any[] = [];
  public timeout;
  public current_inventory: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public statusLoop;
  completeLoading: Loading;
  completeCashInClicked = 0;
  updateLoading: Loading;
  public save1;
  public save2;
  public save3;
  public cancelClicked = 0;
  public global_abcde;
  public updateInventoryCall = 0;
  public machineStatus = false;
  public timerCall;
  public globla_testingd = false;

  constructor(private file: File, private nav: NavController, private alertCtrl: AlertController, private nativeStorage: NativeStorage, private loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
    this.nativeStorage.getItem('Login_Info').then(info => {
      this.id = info.ID;
    });
  }

  ionViewDidLoad() {
    this.amount.cash = '0';
    this.amount.change = '0';
  }

  //when leave the page, trigger function
  ionViewWillLeave() {
    this.requestService.cashinCancelOperation();
  }


  ngOnInit() {
    this.salesForm = new FormGroup({
      get_amount: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{1,3}([.][0-9]{1}[05]{1})?$')]),
      cash: new FormControl('', [Validators.required]),
      change: new FormControl('', [Validators.required])
    });
  }


  public completeCashInBtnOnClick() {
    this.completeCashInClicked = 1;
    (<HTMLInputElement>document.getElementById("backButton")).disabled = false;
    let negativeAmount = (this.u2) - Number(this.amount.get_amount);

    if (negativeAmount < 0) {
      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: 'Not Enough Money.Deposit Awaiting',     //prompt message
        buttons: [
          {
            text: 'OK',
            role: 'dismiss'
          }
        ]
      });
      alert.present();
    }
    else {
      this.completeLoading = this.loadingCtrl.create({
        content: 'Processing...'
      });

      this.completeLoading.present();

      this.statusLoop = setInterval(() => {
      this.checkStatusLoop();
        if(this.machineStatus == true){
          if(this.timerCall == 0)
            this.startTimerEndTransaction();
        }
        else
          this.endTimerEndTransaction();
      }, 500);  //0.5 seconds
    }
  }

  public cancelCashInBtnOnClick() {
    if (this.cancelClicked == 0) {
      this.cancelClicked = 1;
      console.log('Cancel Cash in');
      this.requestService.cashinCancelOperation().then(data => {
        var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:CashinCancelResponse"]["0"].$["n:result"];
        if (statusCode == 0) {
          console.log('success cancel');
          clearInterval(this.loop);
          clearInterval(this.statusLoop);
          this.completeLoading.dismiss();
          this.requestService.requestMachineStatus().then(data => {
            var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
            var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
            if ((dev_id_1 == 2000 && dev_id_2 == 2000)) {
              this.u2 = 0;
              this.amount.cash = '0';
              //this.amount.change = String(((this.u2) - Number(this.amount.get_amount)).toFixed(2));
              this.amount.change = '0';
              (<HTMLInputElement>document.getElementById("backButton")).disabled = true;
              (<HTMLInputElement>document.getElementById("startCashIn-btn")).disabled = true;
              (<HTMLInputElement>document.getElementById("cancelCashIn-btn")).disabled = false;
            }
            else {
              this.amount.get_amount = '';
              this.u2 = 0;
              this.amount.cash = '0';
              //this.amount.change = String(((this.u2) - Number(this.amount.get_amount)).toFixed(2));
              this.amount.change = '0';
              (<HTMLInputElement>document.getElementById("backButton")).disabled = false;
              (<HTMLInputElement>document.getElementById("startCashIn-btn")).disabled = false;
              (<HTMLInputElement>document.getElementById("cancelCashIn-btn")).disabled = true;
            }
          })

        }
        else {
          console.log('failed cancel');
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: 'Unable To Cancel, Please Try Again. <br>Status Code: ' + statusCode,     //prompt message
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'OK',
                role: 'dismiss',
                handler: () => {
                  (<HTMLInputElement>document.getElementById("backButton")).disabled = true;
                  (<HTMLInputElement>document.getElementById("startCashIn-btn")).disabled = true;
                  (<HTMLInputElement>document.getElementById("cancelCashIn-btn")).disabled = false;
                  //(<HTMLInputElement>document.getElementById("completeCashIn-btn")).disabled = false;
                }
              }
            ]
          });
          alert.present();
        }

      });
    }

  }


  public startCashIn() {
    this.cancelClicked = 0;
    this.requestService.refreshSalesTotalOperation(Number(this.amount.get_amount) * 100).then(data => {
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:RefreshSalesTotalResponse"]["0"].$["n:result"];
      if (statusCode == 0) {
        (<HTMLInputElement>document.getElementById("backButton")).disabled = true;
        this.requestService.startCashInOperation().then(data => {
          var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StartCashinResponse"]["0"].$["n:result"];
          if (statusCode == 0) {
            this.loop = setInterval(() => this.countAmount(), 1000);
          }
          else {
            let alert = this.alertCtrl.create({
              title: 'Alert',
              subTitle: statusCode,     //prompt message
              buttons: [
                {
                  text: 'Please Try Again!',
                  role: 'dismiss',
                  //handler: () => {this.resetOperation();}
                }
              ]
            });
            alert.present();
          }
        });
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: 'Unable To Proceed, Please Try Again',     //prompt message
          buttons: [
            {
              text: 'OK',
              role: 'dismiss'
            }
          ]
        });
        alert.present();
      }
    });
  }

  countAmount() {
    this.u = [];
    this.u1 = [];
    this.u2 = 0;
    this.requestService.getCashInAmount().then(cashInData => {
      var dev_id_1 = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      let status = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"]["0"];

      if (status == 3) {
        let bodyLengthCount = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Cash["0"]["Denomination"];
        if (bodyLengthCount == undefined) {
          console.log(bodyLengthCount);
        } else {
          for (let i = 0; i < bodyLengthCount.length; i++) {
            let insertAmount = (cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100;
            let insertPiece = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
            let totalCount = insertAmount * insertPiece;
            this.u[i] = insertAmount;
            this.u1[i] = insertPiece;
            this.u2 += totalCount;
          }
          this.amount.cash = String((this.u2).toFixed(2));
          this.amount.change = String(((this.u2) - Number(this.amount.get_amount)).toFixed(2));
          console.log(this.amount.change);
          console.log((Number(this.amount.change)) >= 0);
          if ((Number(this.amount.change)) >= 0) {
            if (this.completeCashInClicked == 0) {
              console.log('complete complete');
              //(<HTMLInputElement>document.getElementById("completeCashIn-btn")).disabled = false;
              this.completeCashInBtnOnClick();
            }

          }
        }
      }

      console.log(dev_id_1);
      console.log(dev_id_2);

      if ((dev_id_1 == 9400 || dev_id_2 == 9400 || dev_id_1 == 9200 || dev_id_2 == 9200)) {
        clearInterval(this.loop);
        this.nav.setRoot(HomePage);
      }
    });
  }

  checkStatusLoop() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      if ((dev_id_1 == 2000 && dev_id_2 == 2000)) {
          this.machineStatus = true;
      }
      else {
          this.machineStatus = false;
      }
    })
  }

  getStatusOfMachine() {
    this.requestService.requestMachineStatus().then(machineData => {
      this.status = machineData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].$["n:result"];
      this.code = machineData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];

      if (this.status == 0) {            //no error
        if (this.code == 1) {
          (<HTMLInputElement>document.getElementById("startCashIn-btn")).disabled = true;
          (<HTMLInputElement>document.getElementById("cancelCashIn-btn")).disabled = false;
          (<HTMLInputElement>document.getElementById("get_amount")).disabled = true;
          this.startCashIn();
        }              //machine in idle mode

        else {
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: 'Hold On! Someone using the machine',
            buttons: ['Dismiss']
          });
          alert.present();
        }
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          buttons: [
            {
              text: 'Please Try Again!',
              role: 'dismiss',
              //handler: () => {this.resetOperation();}
            }
          ]
        });
        alert.present();
      }
    });
  }

  resetOperation() {
    this.requestService.resetOperation();
  }

  transactionSaveStorage(statusCode) {
    var today = new Date();
    var yy = today.getFullYear();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + (today.getMonth() + 1)).slice(-2);
    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();

    let currentIndex = 0;
    let dateTime = dd + '_' + mm + '_' + hh + '_' + min + '_' + ss;
    let fileName = dateTime;
    let storageFileName = yy + '-' + mm + '-' + dd;;
    let date = dd + '/' + mm + '/' + yy;
    let time = hh + ':' + min + ':' + ss;
    let msg = '';
    if (Number(statusCode) == 0) {
      msg = 'Success';
    }
    else if (Number(statusCode) == 10) {
      msg = 'Change Shortage, No Amount Dispensed. Please Proceed to the Counter'
    }
    else if (Number(statusCode) == 11) {
      msg = 'Device Error'
    }
    else
      msg = statusCode;

    //let test = [{index: 0, id: fileName, date: date, time: time, sales: this.amount.get_amount, insert_amount: this.amount.cash, change: this.amount.change}]
    //this.nativeStorage.setItem('transactionDetails', test);

    this.save1 = this.amount.get_amount;
    this.save2 = this.amount.cash;
    this.save3 = this.amount.change;

    this.nativeStorage.getItem(storageFileName + 'operationDetails').then(data => {
      this.storeArray = [];
      let transactionDetails_length = Number(data.length);
      for (let i = 0; i < transactionDetails_length; i++) {

        this.storeArray.push({ index: i, operation: data[i].operation, date: data[i].date, time: data[i].time, sales: data[i].sales, deposit_details: data[i].deposit_details, total_insert_amount: data[i].total_insert_amount, change: data[i].change, status: data[i].status });
        currentIndex = i + 1;
      }
    }, err => {
      console.log('not existing');
    })

    setTimeout(() => {
      this.storeArray.push({ index: currentIndex, operation: "Sales", date: date, time: time, sales: this.save1, deposit_details: '', total_insert_amount: this.save2, change: this.save3, status: msg });
      this.nativeStorage.setItem(storageFileName + 'operationDetails', this.storeArray);
    }, 1500);


    this.file.checkDir(this.file.externalDataDirectory, 'sales').
      then(_ => {
        console.log('Directory exists');
        this.file.writeFile(
          this.file.externalDataDirectory + '/sales',
          fileName + '.txt',
          'Sales Information\n' +
          '==================================\n' +
          'Status: ' + msg + '\n' +
          'Date: ' + date + '\n' +
          'Time: ' + time + '\n' +
          'Sales (SGD): ' + this.amount.get_amount + '\n' +
          'Inserted Amount (SGD): ' + this.amount.cash + '\n' +
          'Change Amount (SGD): ' + this.amount.change + '\n',
          { replace: false }).
          then(_ => console.log('Success')).
          catch(err => console.log('Failed'));
      }
      ).
      catch(
        err => {
          console.log('Directory doesn\'t exist');
          this.file.createDir(this.file.externalDataDirectory, 'sales', false);
          this.file.writeFile(
            this.file.externalDataDirectory + '/sales',
            fileName + '.txt',
            'Sales Information\n' +
            '==================================\n' +
            'Date: ' + date + '\n' +
            'Time: ' + time + '\n' +
            'Sales (SGD): ' + this.amount.get_amount + '\n' +
            'Inserted Amount (SGD): ' + this.amount.cash + '\n' +
            'Change Amount (SGD): ' + this.amount.change + '\n' +
            'Status: ' + msg + '\n',
            { replace: false }).
            then(_ => console.log('Success')).
            catch(err => console.log('Failed'));
        }
      );
  }

  backButton() {
    this.nav.setRoot(HomePage);
    clearInterval(this.statusLoop);
  }

  startTimerEndTransaction() {
    this.timerCall = 1;
    console.log('start timer end transaction');
    this.timeout = setTimeout(() => {
      clearInterval(this.loop);
      clearInterval(this.statusLoop);
      this.completeLoading.dismiss();
      this.requestService.changeOperation(Number(this.amount.get_amount) * 100).then(event => {
        let statusCode = event["soapenv:Envelope"]["soapenv:Body"]["0"]["n:ChangeResponse"]["0"].$["n:result"];
        let returnStatusLength = event["soapenv:Envelope"]["soapenv:Body"]["0"]["n:ChangeResponse"]["0"].Cash.length;

        if (Number(returnStatusLength) > 0) {
          let totalReturn = 0;

          if (event["soapenv:Envelope"]["soapenv:Body"]["0"]["n:ChangeResponse"]["0"].Cash[1].Denomination != undefined) {
            let checkChangeLength = event["soapenv:Envelope"]["soapenv:Body"]["0"]["n:ChangeResponse"]["0"].Cash[1].Denomination.length;

            for (let j = 0; j < checkChangeLength; j++) {
              let changeType = Number(event["soapenv:Envelope"]["soapenv:Body"]["0"]["n:ChangeResponse"]["0"].Cash[1].Denomination["0"].$["n:fv"]) / 100;
              let changePiece = Number(event["soapenv:Envelope"]["soapenv:Body"]["0"]["n:ChangeResponse"]["0"].Cash[1].Denomination["0"]["n:Piece"]["0"]);
              totalReturn += changeType * changePiece;
            }
          }
        }

        if (statusCode == 0) {

          (<HTMLInputElement>document.getElementById("cancelCashIn-btn")).disabled = true;
          (<HTMLInputElement>document.getElementById("completeCashIn-btn")).disabled = true;

          if (this.amount.cash != '0') {
            this.transactionSaveStorage(statusCode);
            setTimeout(() => {
              document.getElementById('status').innerHTML = "Last Transaction: ";
              document.getElementById('statusSales').innerHTML = "Sales: SGD " + Number(this.amount.get_amount).toFixed(2);
              document.getElementById('statusInsertAmount').innerHTML = "Insert Amount: SGD " + this.amount.cash;
              document.getElementById('statusBalance').innerHTML = "Balance: SGD " + this.amount.change;
              this.amount.cash = '0';
              this.amount.change = '0';
              this.amount.get_amount = '';
              this.completeCashInClicked = 0;

              setTimeout(() => {
                if (this.updateInventoryCall == 0) {
                  this.updateLastestInventory();
                }
              }, 1000);

            }, 1000);
          }
          else{
            this.amount.get_amount = '';
          }
        }
        else if (statusCode == 10) {
          let alert = this.alertCtrl.create({

            title: 'Warning',
            subTitle: "Change Shortage",     //prompt message,
            enableBackdropDismiss: false,
            buttons: [
              {
                text: "OK",
                role: 'dismiss',
                handler: () => {

                  (<HTMLInputElement>document.getElementById("cancelCashIn-btn")).disabled = true;
                  (<HTMLInputElement>document.getElementById("completeCashIn-btn")).disabled = true;

                  this.transactionSaveStorage(statusCode);

                  setTimeout(() => {
                    document.getElementById('status').innerHTML = "Last Transaction: ";
                    document.getElementById('statusSales').innerHTML = "Sales: SGD " + Number(this.amount.get_amount).toFixed(2);
                    document.getElementById('statusInsertAmount').innerHTML = "Insert Amount: SGD " + this.amount.cash;
                    document.getElementById('statusBalance').innerHTML = "Balance: SGD " + this.amount.change;
                    this.amount.cash = '0';
                    this.amount.change = '0';
                    this.amount.get_amount = '';
                    this.completeCashInClicked = 0;
                    this.updateLoading = this.loadingCtrl.create({
                      content: 'Updating...'
                    });

                    this.updateLoading.present();
                    setTimeout(() => {
                      if (this.updateInventoryCall == 0) {
                        this.updateLastestInventory();
                      }
                    }, 3500);
                  }, 1000);

                }
              }
            ]
          });
          alert.present();
        }
        else {
          let alert = this.alertCtrl.create({

            title: 'Warning',
            subTitle: "Error. Status Code: " + statusCode,     //prompt message
            buttons: [
              {
                text: "OK",
                role: 'dismiss'
              }
            ]
          });
          alert.present();
        }
        console.log('Change Operation Response: ' + statusCode);
      });
    }, 2100);
  }

  endTimerEndTransaction() {
    console.log('restart timer end transaction')
    clearTimeout(this.timeout);
    this.timerCall = 0;
    //this.startTimerEndTransaction();
  }

  updateLastestInventory() {
    this.timerCall = 0;
    this.updateInventoryCall = 1;
    this.loading = this.loadingCtrl.create({
      content: 'Updating Inventory...'
    });

    this.loading.present();

    this.current_inventory = [];

    var abc = setInterval(() => {
      this.testingb();
      if (this.global_abcde == true) {
        clearInterval(abc);
        this.testingc();
      }
    }, 500);

  }


  testingb() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      if (dev_id_1 == '1000' && dev_id_2 == '1000') {
        this.global_abcde = true;
      }
      else {
        this.global_abcde = false;
      }
    });
  }


  testingc() {
    console.log('testing c');
    var abcde = setInterval(() => {
      this.testingd();
      if (this.globla_testingd == true) {
        clearInterval(abcde);
        this.testinge();
      }
    }, 1500);

  }

  testingd() {
    this.requestService.inventoryOperation(2).then(sales => {
      let stts = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].$["n:result"];
      if (stts == 0) {
        this.globla_testingd = true;
      }
      else {
        this.globla_testingd = false;
      }
    });
  }

  testinge() {
    this.requestService.inventoryOperation(2).then(sales => {
      console.log('here');
      console.log(sales);
      let body_recycler = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
      for (let j = 0; j < body_recycler.length; j++) {
        let denominationType_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
        let denominationPiece_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
        this.v[j] = ((denominationType_4) / 100).toFixed(2);
        this.v1[j] = denominationPiece_4;

        this.current_inventory.push({ amount: this.v[j], piece: this.v1[j] });

        console.log('new current invent');
        console.log(this.current_inventory);

        setTimeout(() => {
          console.log('new current invent2');
          console.log(this.current_inventory);
          this.nativeStorage.setItem('last_inventory', this.current_inventory).then(
            () => {
              console.log('updated lastest inventory');
              this.updateInventoryCall = 0;
              this.globla_testingd = false;
              this.loading.dismiss();
            },
            error => console.error('Error storing item', error)
          );
        }, 1400);
      }
    });
  }
  
}

