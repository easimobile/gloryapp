import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-recycler',
  templateUrl: 'recycler.html',
})
export class RecyclerPage {
  private isDisabled: boolean = true;
  public u: any[] = [];
  public u1: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public levelAccessCashier: any[] = [];
  public levelAccessAdmin: any[] = [];
  public checkDisable;
  public storeArray: any = [];
  public checkAmount_Piece_recycler: any[] = [];
  public dispenseDetail: any = [];
  public status: any = [];
  public allowToWithdrawPiece: any = [];
  public u2: number = 0;
  loading: Loading;

  constructor(private nav: NavController, private nativeStorage: NativeStorage, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.checkInventory();
  }

  public checkInventory() {
    this.checkAmount_Piece_recycler = [];
    this.u = [];
    this.u1 = [];
    this.requestService.inventoryOperation(2)
      .then(data => {
        let body_cashbox = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];

        /*
        for (let i = 0; i < body_cashbox.length; i++) {
          let denominationType_3 = ((data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100).toFixed(2);
          let denominationPiece_3 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
          this.u[i] = denominationType_3;
          this.u1[i] = denominationPiece_3;
          //this.checkAmount_Piece_recycler.push({ amount: this.u[i], piece: this.u1[i], id: String(i), min: getMinMax[i].min, max: getMinMax[i].max, base: getMinMax[i].base });
          //this.nativeStorage.setItem('configuration', {amount: this.u[i], base: "50", min:"10", max: "30"});
          this.storeArray.push({amount: this.u[i], base: "50", min:"10", max: "30"});
        }
         console.log(this.storeArray);
        this.nativeStorage.setItem('configuration', this.storeArray);
        */

        this.nativeStorage.getItem('configuration').then(getMinMax => {

          for (let i = 0; i < getMinMax.length; i++) {
            let denominationType_3 = ((data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100).toFixed(2);
            let denominationPiece_3 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
            this.u[i] = denominationType_3;
            this.u1[i] = denominationPiece_3;
            this.checkAmount_Piece_recycler.push({ amount: this.u[i], piece: this.u1[i], id: String(i), min: getMinMax[i].min, max: getMinMax[i].max, base: getMinMax[i].base });

          }
          this.checkAmount_Piece_recycler.sort(function (a, b) { return a.amount - b.amount; });
        })
      });
  }


  editMin(clicked_index) {
    this.nativeStorage.getItem('configuration').then(data => {
      let type = "1"; //update min
      let get_amount_native = data[clicked_index].amount
      let get_min_native = data[clicked_index].min;

      let alert = this.alertCtrl.create({
        title: '<center>Edit</center>',
        subTitle: 'Amount Type: (SGD) ' + get_amount_native + '<br> Current Value: ' + get_min_native + '<br><br> New Value: ',
        enableBackdropDismiss: false,
        inputs: [
          {
            type: 'number',
            name: 'min',
            value: get_min_native
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
          },
          {
            text: 'Save',
            handler: data => {
              this.updateConfig(clicked_index, type, data.min);//0
              this.loading = this.loadingCtrl.create({
                content: 'Please wait...',
                dismissOnPageChange: true
              });
              this.loading.present();

              setTimeout(() => {
                this.checkInventory();
                this.loading.dismiss();
              }, 4000);

            }
          }
        ],
      });
      alert.present();
    })
  }

  editMax(clicked_index) {
    this.nativeStorage.getItem('configuration').then(data => {
      let type = "2"; //update max
      let get_amount_native = data[clicked_index].amount
      let get_max_native = data[clicked_index].max;


      let alert = this.alertCtrl.create({
        title: '<center>Edit</center>',
        subTitle: 'Amount Type: (SGD) ' + get_amount_native + '<br> Current Value: ' + get_max_native + '<br><br> New Value: ',
        inputs: [
          {
            type: 'number',
            name: 'max',
            value: get_max_native
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              this.updateConfig(clicked_index, type, data.max);//0
              this.loading = this.loadingCtrl.create({
                content: 'Please wait...',
                dismissOnPageChange: true
              });
              this.loading.present();

              setTimeout(() => {
                this.checkInventory();
                this.loading.dismiss();
              }, 4000);

            }
          }
        ],
      });
      alert.present();
    })

  }

  editBase(clicked_index) {
    this.nativeStorage.getItem('configuration').then(data => {
      let type = "3"; //update base
      let get_amount_native = data[clicked_index].amount
      let get_base_native = data[clicked_index].base;


      let alert = this.alertCtrl.create({
        title: '<center>Edit</center>',
        subTitle: 'Amount Type: (SGD) ' + get_amount_native + '<br> Current Value: ' + get_base_native + '<br><br> New Value: ',
        inputs: [
          {
            type: 'number',
            name: 'base',
            value: get_base_native
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              this.updateConfig(clicked_index, type, data.base);//0
              this.loading = this.loadingCtrl.create({
                content: 'Please wait...',
                dismissOnPageChange: true
              });
              this.loading.present();

              setTimeout(() => {
                this.checkInventory();
                this.loading.dismiss();
              }, 4000);

            }
          }
        ],
      });
      alert.present();
    })

  }

  updateConfig(clicked_index, type, value) {
    this.storeArray = [];
    this.requestService.inventoryOperation(2)
      .then(data => {
        let body_cashbox = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];

        this.nativeStorage.getItem('configuration').then(getMinMax => {

          for (let i = 0; i < getMinMax.length; i++) {
            let denominationType_3 = ((data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100).toFixed(2);
            let denominationPiece_3 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
            this.v[i] = denominationType_3;
            this.v1[i] = denominationPiece_3;
            if (type == 1) {
              if (Number(value) < 1 || value == '' || (Number(value) % 1) != 0) {
                value = getMinMax[clicked_index].min;
              }
              else if (Number(value) > getMinMax[clicked_index].max) {
                value = getMinMax[clicked_index].max;
              }
              getMinMax[clicked_index].min = value;
            }
            else if (type == 2) {
              if (Number(value) < 1 || value == '' || (Number(value) % 1) != 0) {
                value = getMinMax[clicked_index].max;
              }
              else if (Number(value) > 400) {
                value = 400;
              }
              else if (Number(value) < getMinMax[clicked_index].min) {
                value = getMinMax[clicked_index].min;
              }
              getMinMax[clicked_index].max = value;
            }
            else if (type == 3) {
              if ((Number(value) % 1) != 0) {
                value = getMinMax[clicked_index].min;
              }
              else if (Number(value) < Number(getMinMax[clicked_index].min) || value == '') {
                value = getMinMax[clicked_index].min;
              }
              else if (Number(value) > Number(getMinMax[clicked_index].max)) {
                value = getMinMax[clicked_index].max;
              }
              getMinMax[clicked_index].base = value;
            }
            this.storeArray.push({ amount: this.u[i], piece: this.u1[i], id: String(i), min: getMinMax[i].min, max: getMinMax[i].max, base: getMinMax[i].base });
          }
          console.log(this.storeArray);
          this.nativeStorage.setItem('configuration', this.storeArray);
        })
      });
  }

}
