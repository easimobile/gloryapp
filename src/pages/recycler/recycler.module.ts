import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecyclerPage } from './recycler';

@NgModule({
  declarations: [
    RecyclerPage,
  ],
  imports: [
    IonicPageModule.forChild(RecyclerPage),
  ],
})
export class RecyclerPageModule {}
