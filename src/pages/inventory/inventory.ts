import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';

@IonicPage()
@Component({
  selector: 'page-inventory',
  templateUrl: 'inventory.html',
})
export class InventoryPage {
  public u: any[] = [];
  public u1: any[] = [];
  public u2: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public v2: any[] = [];
  public totalPiece: any[] = [];
  public denominationPiece_4: any[] = [];
  public checkAmount_Piece_cashbox: any[] = [];
  public checkAmount_Piece_recycler: any[] = [];
  public tableView;
  public total = 0;
  public totalDisplay;

  constructor(private nav: NavController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
  }

  public checkInventory(table_type) {
    this.total = 0;
    this.checkAmount_Piece_cashbox = [];
    this.u = [];
    this.u1 = [];
    this.u2 = [];
    this.requestService.inventoryOperation(table_type)
      .then(data => {
        console.log(data);
        let body_cashbox = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
      
        for (let i = 0; i < body_cashbox.length; i++) {
          let denominationType_3 = ((data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100).toFixed(2);
          let denominationPiece_3 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
          this.u[i] = denominationType_3;
          this.u1[i] = denominationPiece_3;
          this.u2[i] = (this.u[i] * this.u1[i]).toFixed(2);
          this.total +=  (Number(denominationType_3) * Number(denominationPiece_3));
          this.checkAmount_Piece_cashbox.push({ amount: this.u[i], piece: this.u1[i], total:this.u2[i]});
        }
        this.totalDisplay = (this.total).toFixed(2);
        console.log(this.total);
        this.checkAmount_Piece_cashbox.sort(function (a,b){return a.amount - b.amount;});

      });
  }

  public checkCashBoxInventory() {
    this.total = 0;
    this.checkAmount_Piece_cashbox = [];
    this.u = [];
    this.u1 = [];
    this.v = [];
    this.v1 = [];
    this.requestService.inventoryOperation("?")
      .then(data => {
        console.log(data);
        let body_cashbox = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
        let body_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash[1]["Denomination"];

        for (let i = 0; i < body_cashbox.length; i++) {
          let denominationType_3 = ((data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100).toFixed(2);
          let denominationPiece_3 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
          this.u[i] = denominationType_3;
          this.u1[i] = denominationPiece_3;  
         
          for (let j = 0; j < body_recycler.length; j++) {
            let denominationType_4 = ((data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash[1].Denomination[j].$["n:fv"]) / 100).toFixed(2);
            this.v[j] = denominationType_4;

            if (this.v[j] == this.u[i]) {
              this.denominationPiece_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash[1].Denomination[j]["n:Piece"]["0"];
              this.v1[j] = this.denominationPiece_4;
              this.totalPiece[i] = Number(this.u1[i]) - Number(this.v1[j]);
              break;
            }
            this.totalPiece[i] = this.u1[i];
          }
          this.total +=  (Number(this.u[i]) * Number(this.totalPiece[i]));
          this.totalDisplay = (this.total).toFixed(2);
          this.checkAmount_Piece_cashbox.push({ amount: this.u[i], piece: this.totalPiece[i], total: (this.u[i] * this.totalPiece[i]).toFixed(2)});
        }
        this.checkAmount_Piece_cashbox.sort(function (a,b){return a.amount - b.amount;});
      });
  }

  onChange(selectedValue: any) {
    this.tableView = selectedValue;
    if (this.tableView == 3) {
      this.checkCashBoxInventory();   //inventoryOperation bru:type = 1, cashbox
      document.getElementById("cashbox_total").style.display="block";
      document.getElementById("recycler_total").style.display="none";
      document.getElementById("overall_total").style.display="none";
      document.getElementById("rwd-table_type_recycler").style.display="none";
      document.getElementById("rwd-table_type_cashbox").style.display = "block";
      document.getElementById("rwd-table_type_total").style.display = "none";
      document.getElementById("table_cashbox_title").style.display = "block";
      document.getElementById("table_recycler_title").style.display = "none";
      document.getElementById("table_total_title").style.display = "none";
    }
    else if (this.tableView == 4) {   //recycler
      this.checkInventory(2);   //2 = recycler
      document.getElementById("cashbox_total").style.display="none";
      document.getElementById("recycler_total").style.display="block";
      document.getElementById("overall_total").style.display="none";
      document.getElementById("rwd-table_type_recycler").style.display="block";
      document.getElementById("rwd-table_type_cashbox").style.display = "none";
      document.getElementById("rwd-table_type_total").style.display = "none";
      document.getElementById("table_cashbox_title").style.display = "none";
      document.getElementById("table_recycler_title").style.display = "block";
      document.getElementById("table_total_title").style.display = "none";
    }
    else if (this.tableView == 2) {   //total
      this.checkInventory(1);   // 1 = total
      document.getElementById("cashbox_total").style.display="none";
      document.getElementById("recycler_total").style.display="none";
      document.getElementById("overall_total").style.display="block";
      document.getElementById("rwd-table_type_recycler").style.display="none";
      document.getElementById("rwd-table_type_cashbox").style.display = "none";
      document.getElementById("rwd-table_type_total").style.display = "block";
      document.getElementById("table_cashbox_title").style.display = "none";
      document.getElementById("table_recycler_title").style.display = "none";
      document.getElementById("table_total_title").style.display = "block";
    }

  }
}
