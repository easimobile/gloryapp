import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  operationReportDate = { "selected": "" };
  collectionReportDate = { "selected": "" };
  public reportArray: any[] = [];

  constructor(private file: File, private nav: NavController, private nativeStorage: NativeStorage, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.operationReportLoad('2018-07-09');
  }

  checkDate() {
    var today = new Date();
    var yy = today.getFullYear();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + (today.getMonth() + 1)).slice(-2);

    let currentIndex = 0;
    let dateTime = yy + '-' + mm + '-' + dd;

    return dateTime;
  }

  operationReportLoad(file) {
    this.reportArray = [];
    let currentDate = this.checkDate();
    let fileName;

    if (this.operationReportDate.selected = '') {
      fileName = currentDate;
    }
    else
      fileName = file;

    //document.getElementById("operation_report").style.display = "block";
    //document.getElementById("collection_report").style.display = "none";
    this.nativeStorage.getItem(fileName + 'operationDetails').then(data => {
      console.log(data);
      let transactionDetails_length = data.length;
      for (let i = 7; i < transactionDetails_length; i++) {
        this.reportArray.push({ index: i, operation: data[i].operation, date: data[i].date, time: data[i].time, sales: data[i].sales, deposit_details: data[i].deposit_details, total_insert_amount: data[i].total_insert_amount, change: data[i].change, status: data[i].status });
      }
    })

    console.log(this.reportArray);
    console.log('hereeee');
    console.log(JSON.stringify(this.reportArray));
  
  /*
    this.file.checkDir(this.file.externalDataDirectory, 'report').
    then(_ => {
      console.log('Directory exists');
      this.file.writeFile(
        this.file.externalDataDirectory + '/report',
        fileName + '.txt',
        'Report Information\n' +
        '==================================\n' +
        'Status: ' + msg + '\n' +
        'Date: ' + date + '\n' +
        'Time: ' + time + '\n' +
        'Sales (SGD): ' + this.amount.get_amount + '\n' +
        'Inserted Amount (SGD): ' + this.amount.cash + '\n' +
        'Change Amount (SGD): ' + this.amount.change + '\n',
        { replace: false }).
        then(_ => console.log('Success')).
        catch(err => console.log('Failed'));
    }
    ).
    catch(
      err => {
        console.log('Directory doesn\'t exist');
        this.file.createDir(this.file.externalDataDirectory, 'report', false);
        this.file.writeFile(
          this.file.externalDataDirectory + '/report',
          fileName + '.txt',
          'Report Information\n' +
          '==================================\n' +
          'Date: ' + date + '\n' +
          'Time: ' + time + '\n' +
          'Sales (SGD): ' + this.amount.get_amount + '\n' +
          'Inserted Amount (SGD): ' + this.amount.cash + '\n' +
          'Change Amount (SGD): ' + this.amount.change + '\n' +
          'Status: ' + msg + '\n',
          { replace: false }).
          then(_ => console.log('Success')).
          catch(err => console.log('Failed'));
      }
    );
    */
    console.log(this.reportArray);
  }

  collectionReportLoad(file) {
    this.reportArray = [];
    let currentDate = this.checkDate();
    let fileName;


    if (this.operationReportDate.selected = '') {
      fileName = currentDate;
    }
    else
      fileName = file;

    document.getElementById("operation_report").style.display = "none";
    document.getElementById("collection_report").style.display = "block";
    this.nativeStorage.getItem(fileName + 'operationDetails').then(data => {
      let transactionDetails_length = data.length;
      for (let i = 0; i < transactionDetails_length; i++) {
        this.reportArray.push({ index: i, operation: data[i].operation, date: data[i].date, time: data[i].time, sales: data[i].sales, deposit_details: data[i].deposit_details, total_insert_amount: data[i].total_insert_amount, change: data[i].change, status: data[i].status });
      }
    })
  }

  operationDatePicker() {
    if (this.operationReportDate.selected != '') {
      console.log(this.operationReportDate.selected);
      let file = this.operationReportDate.selected;
      this.operationReportLoad(file);
    }
    else
      console.log('Empty');
  }


  collectionReportDatePicker() {
    if (this.collectionReportDate.selected != '') {
      console.log(this.collectionReportDate.selected);
      let file = this.collectionReportDate.selected;
      this.collectionReportLoad(file);
    }
    else
      console.log('Empty');
  }

  onChange(selectedValue: any) {
    if (selectedValue == 1) {   //operation report
      document.getElementById("operation_datePicker").style.display = "block";
      document.getElementById("collectionReport_datePicker").style.display = "none";
      document.getElementById("operation_report").style.display = "block";
      document.getElementById("collection_report").style.display = "none";
    }
    else if (selectedValue == 2) {  //dispense report
      document.getElementById("operation_datePicker").style.display = "none";
      document.getElementById("collectionReport_datePicker").style.display = "block";
    }
  }

}
