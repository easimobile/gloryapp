import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams, Loading } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {
  ipRegisterCredentials = { ipAddress: '' };
  loading: Loading;
  public ipValidation = new RegExp("(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");
  public tobeExpiredSession;
  public sessionLoop;

  constructor(private nav: NavController, private nativeStorage: NativeStorage, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  // ionViewDidLoad vs ionViewWillEnter, ionViewWillEnter will load after back button, but ionViewDidLoad wont
  ionViewWillEnter() {
    this.nativeStorage.getItem('IPAddress').then(ip => {
      this.ipRegisterCredentials.ipAddress = ip.IP_Address;
    })

  }

  public ipRegister() {
    if (this.ipRegisterCredentials.ipAddress === null) {
      this.showError('Error');
    }
    else if (this.ipValidation.test(this.ipRegisterCredentials.ipAddress) == false) {
      this.showError('Invalid IP Address');
    }
    else {
      this.nativeStorage.setItem('IPAddress', { IP_Address: this.ipRegisterCredentials.ipAddress });
      this.showError('Machine IP Saved');
    }
  }

  showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          role: 'dismiss'
        }
      ]
    });
    alert.present();
  }

}
