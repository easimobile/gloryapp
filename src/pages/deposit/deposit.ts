import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { File } from '@ionic-native/file';
import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-deposit',
  templateUrl: 'deposit.html',
})
export class DepositPage {
  private isDisabled: boolean = true;
  public status: Number = 3;  //default testing
  public code: Number = 3;    //default testing
  public u: any[] = [];
  public u1: any[] = [];
  public u2: number = 0;
  public loop;
  loading: Loading;
  completeLoading: Loading;
  public tableView;
  public autoClose = 0;
  public autoCloseFunction;
  public statusLoop;
  public timeout;
  public cache = 0;
  public current_inventory: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public updateInventoryCall = 0;
  public global_abcde;
  public machineStatus = false;
  public timerCall;

  constructor(private nativeStorage: NativeStorage, private file: File, private nav: NavController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
  }

  ionViewWillLeave() {
    this.requestService.cashinCancelOperation();
  }

  public completeCashInBtnOnClick() {
    this.completeLoading = this.loadingCtrl.create({
      content: 'Processing...'
    });

    this.completeLoading.present();

    clearTimeout(this.autoCloseFunction);

    this.statusLoop = setInterval(() => {
      this.checkStatusLoop();
      if (this.machineStatus == true) {
        if(this.timerCall == 0)
          this.startTimerEndTransaction();
      }
      else {
        this.endTimerEndTransaction();
      }

    }, 500);  //0.5 seconds

  }

  public startCashIn() {
    this.requestService.startCashInOperation().then(data => {
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StartCashinResponse"]["0"].$["n:result"];
      if (statusCode == 0) {
        (<HTMLInputElement>document.getElementById("cancelInsert-btn")).disabled = false;
        (<HTMLInputElement>document.getElementById("backButton")).disabled = false;
        this.loop = setInterval(() => this.countAmount(), 1000);
        this.startTimerAutoClose();
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: statusCode,     //prompt message
          buttons: [
            {
              text: 'Please Try Again!',
              role: 'dismiss',
              //handler: () => {this.resetOperation();}
            }
          ]
        });
        alert.present();
      }
    });
  }

  countAmount() {
    this.u = [];
    this.u1 = [];
    this.u2 = 0;
    this.requestService.getCashInAmount().then(cashInData => {
      var dev_id_1 = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      let status = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"]["0"];

      if (status == 3) {
        let bodyLengthCount = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Cash["0"]["Denomination"];
        if (bodyLengthCount == undefined) {

        } else {
          for (let i = 0; i < bodyLengthCount.length; i++) {
            let insertAmount = (cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100;
            let insertPiece = cashInData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];
            let totalCount = insertAmount * insertPiece;
            this.u[i] = insertAmount;
            this.u1[i] = insertPiece;
            this.u2 += totalCount;
          }

          document.getElementById('insertReceivedCash').innerHTML = String((this.u2).toFixed(2));

          if ((this.u2) > 0) {
            (<HTMLInputElement>document.getElementById("completeInsert-btn")).disabled = false;
            (<HTMLInputElement>document.getElementById("cancelInsert-btn")).disabled = true;
            (<HTMLInputElement>document.getElementById("backButton")).disabled = true;
          }

        }
      }
      console.log(dev_id_1);
      console.log(dev_id_2);

      if ((dev_id_1 == 9400 || dev_id_2 == 9400 || dev_id_1 == 9200 || dev_id_2 == 9200)) {
        clearInterval(this.loop);
        this.nav.setRoot(HomePage);
      }
      else if (dev_id_1 != 2000 || dev_id_2 != 2000) {
        this.endTimerAutoClose();
      }

    });
  }

  getStatusOfMachine() {
    this.requestService.requestMachineStatus().then(machineData => {
      this.status = machineData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].$["n:result"];
      this.code = machineData["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];

      if (this.status == 0) {            //no error
        if (this.code == 1) {
          this.startCashIn();
          (<HTMLInputElement>document.getElementById("startInsert-btn")).disabled = true;
        }

        else {
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: 'Hold On!',
            buttons: ['Dismiss']
          });
          alert.present();
        }
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          buttons: [
            {
              text: 'Please Try Again!',
              role: 'dismiss',
              //handler: () => {this.resetOperation();}
            }
          ]
        });
        alert.present();
      }
    });
  }

  backButton() {
    clearInterval(this.loop);
    clearTimeout(this.autoCloseFunction);
    clearInterval(this.statusLoop);
    this.nav.setRoot(HomePage);
  }

  cashinCancelOperation() {
    clearInterval(this.loop);
    clearTimeout(this.autoCloseFunction);
    clearInterval(this.statusLoop);
    this.requestService.cashinCancelOperation();
    (<HTMLInputElement>document.getElementById("startInsert-btn")).disabled = false;
    (<HTMLInputElement>document.getElementById("cancelInsert-btn")).disabled = true;

  }

  checkStatusLoop() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];
      if ((dev_id_1 == 2000 && dev_id_2 == 2000)) {
        this.machineStatus = true;
      }
      else {
        this.machineStatus = false;
      }
    })
  }

  transactionSaveStorage(endCashinResponse) {
    var today = new Date();
    var yy = today.getFullYear();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + (today.getMonth() + 1)).slice(-2);
    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();
    let storeArray: any[] = [];
    let depositArray: any[] = [];
    let currentIndex = 0;
    let dateTime = dd + '_' + mm + '_' + hh + '_' + min + '_' + ss;
    let fileName = dateTime;
    let storageFileName = yy + '-' + mm + '-' + dd;
    let date = dd + '/' + mm + '/' + yy;
    let time = hh + ':' + min + ':' + ss;
    /*
    let msg = '';
    if (Number(statusCode) == 0) {
      msg = 'Success';
    }
    else if (Number(statusCode) == 10) {
      msg = 'Change Shortage, No Amount Dispensed'
    }
    else
      msg = statusCode;
    */

    storeArray = [];

    console.log(endCashinResponse);
    let insert_amount_length = endCashinResponse["soapenv:Envelope"]["soapenv:Body"]["0"]["n:EndCashinResponse"]["0"].Cash["0"].Denomination.length;
    for (let i = 0; i < insert_amount_length; i++) {
      let amount = (Number(endCashinResponse["soapenv:Envelope"]["soapenv:Body"]["0"]["n:EndCashinResponse"]["0"].Cash["0"].Denomination[i].$["n:fv"]) / 100).toFixed(2);
      let piece = endCashinResponse["soapenv:Envelope"]["soapenv:Body"]["0"]["n:EndCashinResponse"]["0"].Cash["0"].Denomination[i]["n:Piece"]["0"];

      storeArray.push({ amount: amount, piece: piece });
    }

    console.log(storageFileName);
    this.nativeStorage.getItem(storageFileName + 'operationDetails').then(data => {
      depositArray = [];
      let depositDetails_length = Number(data.length);
      for (let i = 0; i < depositDetails_length; i++) {
        depositArray.push({ index: i, operation: data[i].operation, date: data[i].date, time: data[i].time, sales: data[i].sales, deposit_details: data[i].deposit_details, total_insert_amount: data[i].total_insert_amount, change: data[i].change, status: data[i].status });
        currentIndex = i + 1;
      }
    }, err => {
      console.log('not existing');
    })

    setTimeout(() => {
      depositArray.push({ index: currentIndex, operation: "Deposit", date: date, time: time, sales: '', deposit_details: JSON.stringify(storeArray), total_insert_amount: String((this.u2).toFixed(2)), change: '', status: 'Success' });
      this.nativeStorage.setItem(storageFileName + 'operationDetails', depositArray);
    }, 1500);

    console.log(depositArray);
    this.file.checkDir(this.file.externalDataDirectory, 'deposit').
      then(_ => {
        console.log('Directory exists');
        this.file.writeFile(
          this.file.externalDataDirectory + '/deposit',
          fileName + '.txt',
          'Deposit Information\n' +
          '==================================\n' +
          //'Status: ' + msg + '\n' +
          'Date: ' + date + '\n' +
          'Time: ' + time + '\n' +
          'Inserted Info: ' + JSON.stringify(storeArray) + '\n' +
          'Total Inserted Amount (SGD): ' + String((this.u2).toFixed(2)) + '\n',
          { replace: false }).
          then(_ => console.log('Success')).
          catch(err => console.log('Failed'));
      }
      ).
      catch(
        err => {
          console.log('Directory doesn\'t exist');
          this.file.createDir(this.file.externalDataDirectory, 'deposit', false);
          this.file.writeFile(
            this.file.externalDataDirectory + '/deposit',
            fileName + '.txt',
            'Deposit Information\n' +
            '==================================\n' +
            //'Status: ' + msg + '\n' +
            'Date: ' + date + '\n' +
            'Time: ' + time + '\n' +
            'Inserted Info: ' + storeArray + '\n' +
            'Inserted Amount (SGD): ' + String((this.u2).toFixed(2)) + '\n',
            { replace: false }).
            then(_ => console.log('Success')).
            catch(err => console.log('Failed'));
        }
      );

  }

  startTimerEndTransaction() {
    this.timerCall = 1;
    console.log('start timer end transaction');
    this.timeout = setTimeout(() => {
      clearInterval(this.statusLoop);
      clearInterval(this.loop);
      this.completeLoading.dismiss();
      this.requestService.endCashInOperation().then(data => {
        console.log('End Cash in response');
        this.transactionSaveStorage(data);
      });

      clearTimeout(this.timeout);
      setTimeout(() => {
        document.getElementById('insertReceivedCash').innerHTML = "0";
        (<HTMLInputElement>document.getElementById("completeInsert-btn")).disabled = true;
        (<HTMLInputElement>document.getElementById("startInsert-btn")).disabled = false;
        (<HTMLInputElement>document.getElementById("backButton")).disabled = false;
        (<HTMLInputElement>document.getElementById("cancelInsert-btn")).disabled = true;
        let positiveAmount = String((this.u2).toFixed(2));
        document.getElementById('status').innerHTML = "Last Inserted Amount: SGD " + positiveAmount;
        let alert = this.alertCtrl.create({
          title: 'Total Inserted Amount: ',
          subTitle: "SGD " + positiveAmount,     //prompt message
          enableBackdropDismiss: false,
          buttons: [
            {
              text: "OK",
              role: 'dismiss',
              handler: () => {
                this.cache = 0;
                if (this.updateInventoryCall == 0) {
                  this.updateLastestInventory();
                }
              }
            }
          ]
        });
        alert.present();
      }, 300);
    }, 2100);
  }

  endTimerEndTransaction() {
    console.log('restart timer end transaction')
    clearTimeout(this.timeout);
    this.timerCall = 0;
    //this.startTimerEndTransaction();
  }

  startTimerAutoClose() {
    console.log('start timer auto close')
    this.autoCloseFunction = setTimeout(() => {
      this.completeCashInBtnOnClick();
    }, 60000);   //after one minute will auto close the cash in operation
  }

  endTimerAutoClose() {
    console.log('cancel and restart auto clos')
    clearTimeout(this.autoCloseFunction);
    this.startTimerAutoClose();
  }


  updateLastestInventory() {
    this.timerCall = 0;
    this.updateInventoryCall = 1;
    this.loading = this.loadingCtrl.create({
      content: 'Updating Inventory...'
    });

    this.loading.present();

    this.current_inventory = [];

    var abc = setInterval(() => {
      this.testingb();
      if (this.global_abcde == true) {
        clearInterval(abc);
        this.testingc();
      }
    }, 500);

  }


  testingb() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      if (dev_id_1 == '1000' && dev_id_2 == '1000') {
        this.global_abcde = true;
      }
      else {
        this.global_abcde = false;
      }
    });
  }

  testingc() {
    console.log('testing c');
    var abcde = setInterval(() => {
      this.testingd();
      if (this.testingd() == true) {
        clearInterval(abcde);
        this.testinge();
      }
    }, 1500);

  }

  testingd() {
    var sts = true;
    this.requestService.inventoryOperation(2).then(sales => {
      let stts = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].$["n:result"];
      if (stts == '10') {
        sts = true;
      }
      else {
        sts = false;
      }
    });
    return sts;
  }

  testinge() {
    this.requestService.inventoryOperation(2).then(sales => {
      console.log('here');
      console.log(sales);
      let body_recycler = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
      for (let j = 0; j < body_recycler.length; j++) {
        let denominationType_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
        let denominationPiece_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
        this.v[j] = ((denominationType_4) / 100).toFixed(2);
        this.v1[j] = denominationPiece_4;

        this.current_inventory.push({ amount: this.v[j], piece: this.v1[j] });

        console.log('new current invent');
        console.log(this.current_inventory);

        setTimeout(() => {
          console.log('new current invent2');
          console.log(this.current_inventory);
          this.nativeStorage.setItem('last_inventory', this.current_inventory).then(
            () => {
              console.log('updated lastest inventory');
              this.updateInventoryCall = 0;
              this.loading.dismiss();
            },
            error => console.error('Error storing item', error)
          );
        }, 1400);
      }
    });
  }
}
