import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {
  private isDisabled: boolean = true;
  sessionSettingForm: FormGroup;
  amount = { "new_time": "", "existing_time": "" };
  public u: any[] = [];
  public u1: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public levelAccessCashier: any[] = [];
  public levelAccessAdmin: any[] = [];
  public checkDisable;
  public storeArray: any = [];
  public checkAmount_Piece_cashbox: any[] = [];
  public checkAmount_Piece_recycler: any[] = [];
  public dispenseDetail: any = [];
  public status: any = [];
  public allowToWithdrawPiece: any = [];
  public u2: number = 0;
  loading: Loading;
  public type100 = 0;
  public type10 = 0;
  public type5 = 0;
  public type2 = 0;
  public type1 = 0;
  public type050 = 0;
  public type020 = 0;
  public type010 = 0;
  public type005 = 0;
  public storeArrayTest1: any[] = [];
  public storeArrayTest2: any[] = [];

  //cashier
  public test = [{ level: "001", id: "0", name: "Replenishment", control: "1", icon_name: "add-circle" },
  { level: "001", id: "1", name: "Sales", control: "1", icon_name: "cash" },
  { level: "001", id: "2", name: "Dispense", control: "1", icon_name: "sync" },
  { level: "001", id: "3", name: "Refund", control: "1", icon_name: "redo" },
  { level: "001", id: "4", name: "Inventory", control: "1", icon_name: "list-box" },
  { level: "001", id: "5", name: "Push", control: "1", icon_name: "git-compare" },
  { level: "001", id: "6", name: "Lock/Unlock", control: "1", icon_name: "lock" },
  { level: "001", id: "7", name: "Report", control: "1", icon_name: "copy" },
  { level: "001", id: "8", name: "Create/View User", control: "1", icon_name: "person" },
  { level: "001", id: "9", name: "Recycler Box configuration", control: "1", icon_name: "settings" },
  { level: "001", id: "10", name: "Coin Return", control: "1", icon_name: "return-left" },
  { level: "001", id: "11", name: "User Level Configuration", control: "1", icon_name: "walk" },
  { level: "001", id: "12", name: "Faulty Info", control: "1", icon_name: "close-circle" },
  { level: "001", id: "13", name: "Reset", control: "1", icon_name: "refresh-circle" },
  //{ level: "001", id: "14", name: "Reboot Machine", control: "1", icon_name: "repeat" },
  { level: "001", id: "15", name: "Logout", control: "1", icon_name: "log-out" }];

  //admin
  public test2 = [{ level: "002", id: "0", name: "Replenishment", control: "1", icon_name: "add-circle" },
  { level: "002", id: "1", name: "Sales", control: "1", icon_name: "cash" },
  { level: "002", id: "2", name: "Dispense", control: "1", icon_name: "sync" },
  { level: "002", id: "3", name: "Refund", control: "1", icon_name: "redo" },
  { level: "002", id: "4", name: "Inventory", control: "1", icon_name: "list-box" },
  { level: "002", id: "5", name: "Push", control: "1", icon_name: "git-compare" },
  { level: "002", id: "6", name: "Lock/Unlock", control: "1", icon_name: "lock" },
  { level: "002", id: "7", name: "Report", control: "1", icon_name: "copy" },
  { level: "002", id: "8", name: "Create/View User", control: "1", icon_name: "person" },
  { level: "002", id: "9", name: "Recycler Box configuration", control: "1", icon_name: "settings" },
  { level: "002", id: "10", name: "Coin Return", control: "1", icon_name: "return-left" },
  { level: "002", id: "11", name: "User Level Configuration", control: "1", icon_name: "walk" },
  { level: "002", id: "12", name: "Faulty Info", control: "1", icon_name: "close-circle" },
  { level: "002", id: "13", name: "Reset", control: "1", icon_name: "refresh-circle" },
  //{ level: "002", id: "14", name: "Reboot Machine", control: "1", icon_name: "repeat" },
  { level: "002", id: "15", name: "Logout", control: "1", icon_name: "log-out" }];

  //super user
  public test3 = [{ level: "003", id: "0", name: "Replenishment", control: "1", icon_name: "add-circle" },
  { level: "003", id: "1", name: "Sales", control: "1", icon_name: "cash" },
  { level: "003", id: "2", name: "Dispense", control: "1", icon_name: "sync" },
  { level: "003", id: "3", name: "Refund", control: "1", icon_name: "redo" },
  { level: "003", id: "4", name: "Inventory", control: "1", icon_name: "list-box" },
  { level: "003", id: "5", name: "Push", control: "1", icon_name: "git-compare" },
  { level: "003", id: "6", name: "Lock/Unlock", control: "1", icon_name: "lock" },
  { level: "003", id: "7", name: "Report", control: "1", icon_name: "copy" },
  { level: "003", id: "8", name: "Create/View User", control: "1", icon_name: "person" },
  { level: "003", id: "9", name: "Recycler Box Configuration", control: "1", icon_name: "settings" },
  { level: "003", id: "10", name: "Coin Return", control: "1", icon_name: "return-left" },
  { level: "003", id: "11", name: "User Level Configuration", control: "1", icon_name: "walk" },
  { level: "003", id: "12", name: "Faulty Info", control: "1", icon_name: "close-circle" },
  { level: "003", id: "13", name: "Reset", control: "1", icon_name: "refresh-circle" },
  //{ level: "003", id: "14", name: "Reboot Machine", control: "1", icon_name: "repeat" },
  { level: "003", id: "15", name: "Logout", control: "1", icon_name: "log-out" }];

  constructor(private nav: NavController, private nativeStorage: NativeStorage, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    this.checkUserSetting();
    //this.nativeStorage.setItem('level_control_cashier', this.test);
    //this.nativeStorage.setItem('level_control_admin', this.test2);
    //this.nativeStorage.setItem('level_control_su', this.test3);
    //this.nativeStorage.setItem('session_time', {time:"5"});
    //console.log(this.test);
    this.nativeStorage.getItem('session_time').then(time => {
      this.amount.existing_time = time.time;
    })
  }

  ngOnInit() {
    this.sessionSettingForm = new FormGroup({
      new_time: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{1,2}?$')]),
      existing_time: new FormControl('', [Validators.required])
    });
  }

  public checkUserSetting() {
    this.levelAccessCashier = [];
    this.levelAccessAdmin = [];

    this.nativeStorage.getItem('level_control_cashier').then(getLevelSetting => {
      for (let i = 0; i < getLevelSetting.length; i++) {
        this.levelAccessCashier.push({ level: getLevelSetting[i].level, id: getLevelSetting[i].id, name: getLevelSetting[i].name, control: getLevelSetting[i].control,icon_name :  getLevelSetting[i].icon_name});
      }
    })

    this.nativeStorage.getItem('level_control_admin').then(getLevelSetting => {
      for (let i = 0; i < getLevelSetting.length; i++) {
        this.levelAccessAdmin.push({ level: getLevelSetting[i].level, id: getLevelSetting[i].id, name: getLevelSetting[i].name, control: getLevelSetting[i].control, icon_name :  getLevelSetting[i].icon_name});
      }
    })
  }

  updateLevelCashier(id, value) {
    this.storeArrayTest2 = [];
    for (let i = 0; i < this.levelAccessCashier.length; i++) {
      if (this.levelAccessCashier[i].id == id) {
        this.levelAccessCashier[i].control = String(value);
      }
      this.storeArrayTest2.push({ level: this.levelAccessCashier[i].level, id: this.levelAccessCashier[i].id, name: this.levelAccessCashier[i].name, control: this.levelAccessCashier[i].control, icon_name : this.levelAccessCashier[i].icon_name});
    }
  }

  updateLevelAdmin(id, value) {
    this.storeArrayTest1 = [];
    for (let i = 0; i < this.levelAccessAdmin.length; i++) {
      if (this.levelAccessAdmin[i].id == id) {
        this.levelAccessAdmin[i].control = String(value);
      }
      this.storeArrayTest1.push({ level: this.levelAccessAdmin[i].level, id: this.levelAccessAdmin[i].id, name: this.levelAccessAdmin[i].name, control: this.levelAccessAdmin[i].control, icon_name : this.levelAccessAdmin[i].icon_name});
    }
  }

  saveChange(type) {
    if (type == 1) {
      this.nativeStorage.setItem('level_control_cashier', this.storeArrayTest2);
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange: true
      });
      this.loading.present();

      setTimeout(() => {
        this.nav.setRoot(HomePage);
        this.loading.dismiss();
      }, 3000);
    }
    else if (type == 2) {
      this.nativeStorage.setItem('level_control_admin', this.storeArrayTest1);
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange: true
      });
      this.loading.present();

      setTimeout(() => {
        this.nav.setRoot(HomePage);
        this.loading.dismiss();
      }, 3000);
    }

  }

  sessionSetting() {
    this.nativeStorage.setItem('session_time', { time: this.amount.new_time });
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();

    setTimeout(() => {
      this.amount.existing_time = this.amount.new_time;
      this.amount.new_time = '';
      this.loading.dismiss();
    }, 1000);
    this.amount.existing_time = this.amount.new_time;
  }

  onChange(selectedValue: any) {
    if (selectedValue == 1) {
      document.getElementById("user_table_admin").style.display = "block";
      document.getElementById("user_table_cashier").style.display = "none";
      document.getElementById("sessionSettingForm").style.display = "none";
    }
    else if (selectedValue == 2) {
      document.getElementById("user_table_admin").style.display = "none";
      document.getElementById("user_table_cashier").style.display = "block";
      document.getElementById("sessionSettingForm").style.display = "none";
    }
    else if (selectedValue == 3) {
      document.getElementById("user_table_admin").style.display = "none";
      document.getElementById("user_table_cashier").style.display = "none";
      document.getElementById("sessionSettingForm").style.display = "block";
    }
  }
}
