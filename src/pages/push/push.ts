import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { NativeStorage } from '@ionic-native/native-storage';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-push',
  templateUrl: 'push.html',
})
export class PushPage {
  private isDisabled: boolean = true;
  public u: any[] = [];
  public u1: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public checkDisable;
  public storeArrayPush: any = [];
  public storeArrayPushAll: any = [];
  public storeArrayPushBase: any = [];
  public toPushDetails: any = [];
  public checkAmount_Piece_cashbox: any[] = [];
  public checkAmount_Piece_recycler: any[] = [];
  public pushDetail: any = [];
  public allowToWithdrawId: any = [];
  public allowToWithdrawPiece: any = [];
  public tableView;
  public loading;
  public statusLoop;
  pushForm: FormGroup;
  type_recycler = { "pieceInput": "" };
  public current_inventory: any[] = [];
  public updateInventoryCall = 0;
  public global_abcde;
  public setRootCall = 0;
  public status = false;

  constructor(public loadingCtrl: LoadingController, private nav: NavController, private nativeStorage: NativeStorage, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
  }

  ngOnInit() {
    this.pushForm = new FormGroup({
      //pieceInput: new FormControl('', [Validators.required, Validators.pattern('^([1-9][0-9]{0,2}|1000)$')])
      pieceInput: new FormControl('')
    });
  }

  ionViewDidLoad() {
    this.checkInventory();
  }

  public checkInventory() {
    this.checkAmount_Piece_recycler = [];
    this.pushDetail = [];
    this.storeArrayPush = [];
    this.storeArrayPushAll = [];
    this.storeArrayPushBase = [];
    //inventoryOperation bru:type = 2, recycler, 1 = all, cashbox = 1-2
    this.requestService.inventoryOperation(2)
      .then(data => {
        let body_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
        this.nativeStorage.getItem('configuration').then(getConfigInfo => {

          for (let j = 0; j < body_recycler.length; j++) {
            let denominationType_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
            let denominationPiece_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
            let denominationDevid_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:devid"];
            let basePiece = getConfigInfo[j].base;
            let toPushBasePiece = denominationPiece_4 - basePiece;

            if (toPushBasePiece < 0) {
              toPushBasePiece = 0;
            }

            this.v[j] = ((denominationType_4) / 100).toFixed(2);
            this.v1[j] = denominationPiece_4;
            this.checkDisable = true;

            if (this.v1[j] != 0) {
              this.checkDisable = false;
              this.storeArrayPush.push({ id: j, type: denominationType_4, devid: denominationDevid_4, current: this.v1[j] });
            }

            this.storeArrayPushAll.push({ id: j, type: denominationType_4, devid: denominationDevid_4, piece: denominationPiece_4 });
            this.storeArrayPushBase.push({ id: j, type: denominationType_4, devid: denominationDevid_4, piece: String(toPushBasePiece) });
            this.pushDetail.push({ id: j, value: '', disabled: this.checkDisable });
            this.checkAmount_Piece_recycler.push({ amount: this.v[j], piece: this.v1[j], id: String(j), isDisabled: this.checkDisable });
          }
          this.checkAmount_Piece_recycler.sort(function (a, b) { return a.amount - b.amount; });
        })
      });

  }

  public pushConfirmation(type) {
    let alert = this.alertCtrl.create({
      title: 'Push Warning',
      message: 'Are You Sure To Push?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.pushToCashBox(type);
            (<HTMLInputElement>document.getElementById("submit-btn")).disabled = true;
          }
        }
      ]
    });
    alert.present();
  }

  public pushToCashBox(type) {
    //have to error handling of no more than maximum piece
    let failed = 0;
    for (let i = 0; i < this.storeArrayPush.length; i++) {
      this.allowToWithdrawId = this.storeArrayPush[i].id;
      this.allowToWithdrawPiece[i] = this.pushDetail[this.allowToWithdrawId].value;
      console.log(this.allowToWithdrawPiece[i]);
      if(this.allowToWithdrawPiece[i] % 1 != 0){
        failed = 1;
      }

      if (this.allowToWithdrawPiece[i] < 1) {
        this.allowToWithdrawPiece[i] = 0;
      }

      if (Number(this.storeArrayPush[i].current) < Number(this.allowToWithdrawPiece[i])) {
        failed = 1;
      }
      this.storeArrayPush[i].piece = this.allowToWithdrawPiece[i];
    }

    if (type == 1) {   //push by selection
      this.toPushDetails = this.storeArrayPush;
    }
    else if (type == 2) { //push all
      this.toPushDetails = this.storeArrayPushAll;
    }
    else if (type == 3) { //push by base
      this.toPushDetails = this.storeArrayPushBase;
    }
    console.log(failed)
    console.log(this.toPushDetails);
    //check result(failed or success)

    if (failed == 1) {
      let alert = this.alertCtrl.create({
        title: 'Failed To Push',
        subTitle: 'Invalid amount detected.',     //prompt message
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            role: 'dismiss',
            handler: () => {
              (<HTMLInputElement>document.getElementById("submit-btn")).disabled = false;
            }
          }
        ]
      });
      alert.present();
    } else {
      this.requestService.pushToCashBoxOperation(this.toPushDetails).then(data => {
        var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:CollectResponse"]["0"].$["n:result"];
        if (statusCode == "0") {
          let loop = setInterval(() => {
            this.checkStatusLoop();
            if(this.status == true){
              clearInterval(loop);
              this.updateLastestInventory();
              this.checkInventory();
            }
          }, 1000);  //1 seconds
        }
        else {
          let alert = this.alertCtrl.create({
            title: 'Failed To Push (Cash Box Full)',
            subTitle: 'Please Clear the CashBox to Perform Another Push.',     //prompt message
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'OK.',
                role: 'dismiss',
                handler: () => {
                  (<HTMLInputElement>document.getElementById("submit-btn")).disabled = false;
                }
              }
            ]
          });
          alert.present();
        }


      });
    }
    
  }

  public checkStatusLoop() {

    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];

      if ((dev_id_1 == 1000 && dev_id_2 == 1000)) {
        this.status = true;
      }
      else {
       this.status = false

      }
    })
  }

  public resetBtnOnClick() {
    this.pushForm.reset();
  }

  updateLastestInventory() {
    this.updateInventoryCall = 1;
    this.loading = this.loadingCtrl.create({
      content: 'Updating Inventory...'
    });

    this.loading.present();

    this.current_inventory = [];

    var abc = setInterval(() => {
      this.testingb();
      if (this.global_abcde == true) {
        clearInterval(abc);
        this.testingc();
      }
    }, 500);

  }


  testingb() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      if (dev_id_1 == '1000' && dev_id_2 == '1000') {
        this.global_abcde = true;
      }
      else {
        this.global_abcde = false;
      }
    });
  }


  testingc() {
    console.log('testing c');
    var abcde = setInterval(() => {
      this.testingd();
      if (this.testingd() == true) {
        clearInterval(abcde);
        this.testinge();
      }
    }, 1500);

  }

  testingd() {
    var sts = true;
    this.requestService.inventoryOperation(2).then(sales => {
      let stts = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].$["n:result"];
      if (stts == '10') {
        sts = true;
      }
      else {
        sts = false;
      }
    });
    return sts;
  }

  testinge() {
    this.requestService.inventoryOperation(2).then(sales => {
      console.log('here');
      console.log(sales);
      let body_recycler = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
      for (let j = 0; j < body_recycler.length; j++) {
        let denominationType_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
        let denominationPiece_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
        this.v[j] = ((denominationType_4) / 100).toFixed(2);
        this.v1[j] = denominationPiece_4;

        this.current_inventory.push({ amount: this.v[j], piece: this.v1[j] });

        console.log('new current invent');
        console.log(this.current_inventory);

        setTimeout(() => {
          console.log('new current invent2');
          console.log(this.current_inventory);
          this.nativeStorage.setItem('last_inventory', this.current_inventory).then(
            () => {
              console.log('updated lastest inventory');
              if(this.setRootCall == 0){
                this.setRootCall = 1;
                this.navCtrl.setRoot(HomePage);
              }
              this.updateInventoryCall = 0;
              this.loading.dismiss();
            },
            error => console.error('Error storing item', error)
          );
        }, 1400);
      }
    });
  }
}
