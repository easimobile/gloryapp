import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';

@IonicPage()
@Component({
  selector: 'page-unlock',
  templateUrl: 'unlock.html',
})
export class UnlockPage {

  constructor(private nav: NavController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
  }

  public lockOperation(checkLockType) {
    
    this.requestService.lockUnitOperation(checkLockType).then(data => {
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:LockUnitResponse"]["0"].$["n:result"];
      if (statusCode != "0") {
        console.log(statusCode)
      }
    });


  }

  public unlockOperation(checkUnlockType) {
    this.requestService.unLockUnitOperation(checkUnlockType).then(data => {
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:UnLockUnitResponse"]["0"].$["n:result"];
      if (statusCode != "0") {
        console.log(statusCode)
      }

    });

    setTimeout(() => {
      this.lockOperation(checkUnlockType);
    }, 30000);
  }

}
