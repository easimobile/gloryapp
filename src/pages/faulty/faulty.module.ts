import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaultyPage } from './faulty';

@NgModule({
  declarations: [
    FaultyPage,
  ],
  imports: [
    IonicPageModule.forChild(FaultyPage),
  ],
})
export class FaultyPageModule {}
