import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController, Loading } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  createUserForm: FormGroup;
  userData = { "name": "", "password": "", "user_level": "", "user_id": "" };
  public pass;
  private isDisabled: boolean = true;
  loading: Loading;
  public user_profile: any[] = [];
  //public test = [{name: "Super User", userid: "001", pass: "abc12345", level: "003", status: "1"}];

  constructor(private nav: NavController, private nativeStorage: NativeStorage, private loadingCtrl: LoadingController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad(){
    //this.nativeStorage.setItem('user_profile',this.test);
    //console.log(this.test);
    //this.nativeStorage.remove('user_profile');

    this.nativeStorage.getItem('user_profile').then(data => {
      let user_count = data.length;
      console.log(data);
      let temp_id = (Number(data[user_count - 1].userid)) + 1;
      let p = temp_id.toString().length;
      if (p == 1)
        this.userData.user_id = "00" + String(temp_id);
      else if (p == 2)
        this.userData.user_id = "0" + String(temp_id);
      else if (p == 3)
        this.userData.user_id = String(temp_id);
    });
    (<HTMLInputElement>document.getElementById("user_id")).disabled = true;
    this.userProfileLoad();
  }


  ngOnInit() {
    //let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.createUserForm = new FormGroup({
      //name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(10)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
      user_level: new FormControl('', [Validators.required]),
      user_id: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(4), Validators.maxLength(30)])
      //email: new FormControl('', [Validators.required, Validators.pattern(EMAILPATTERN)]),
    });
  }

  generatePassword() {
    this.pass = Math.random().toString(36).slice(-6);
    this.userData.password = this.pass;
  }

  public createUser() {
    let get_user_name = this.userData.name;
    let get_user_id = this.userData.user_id;
    let get_pass = this.userData.password;
    let get_user_level = this.userData.user_level;
    let get_user_status = "1";
    let test: any[] = [];
    let new_user_array = [{ name: get_user_name, userid: get_user_id, pass: get_pass, level: get_user_level, status: get_user_status }];

    //this.nativeStorage.setItem('user_profile', new_user_array);

    this.nativeStorage.getItem('user_profile').then(data => {
      data.push(new_user_array["0"]);
      this.nativeStorage.setItem('user_profile', data);
      this.createUserForm.reset();
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        dismissOnPageChange: true
      });
      this.loading.present();

      setTimeout(() => {
        this.loading.dismiss();
        this.get_current_id();
      }, 3000);

    },
      error => {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: 'Failed To Create User.',
          buttons: [
            {
              text: 'OK',
              role: 'dismiss'
            }
          ]
        });
        alert.present();
      })
  }

  userProfileLoad() {
    this.user_profile = [];
    this.nativeStorage.getItem('user_profile').then(data => {
      let user_count = data.length;
      for (let i = 0; i < user_count; i++) {
        let get_user_name_native = data[i].name;
        let get_user_id_native = data[i].userid;
        let get_pass_native = data[i].pass;
        let get_user_status_native = data[i].status;
        let get_user_level_native = data[i].level;
        let index = i;
        this.user_profile.push({ name: get_user_name_native, userid: get_user_id_native, pass: get_pass_native, level: get_user_level_native, status: get_user_status_native, index: index });
      }
    })
  }

  get_current_id() {
    this.nativeStorage.getItem('user_profile').then(data => {
      let user_count = data.length;
      let temp_id = (Number(data[user_count - 1].userid)) + 1;
      let p = temp_id.toString().length;
      if (p == 1)
        this.userData.user_id = "00" + String(temp_id);
      else if (p == 2)
        this.userData.user_id = "0" + String(temp_id);
      else if (p == 3)
        this.userData.user_id = String(temp_id);
    })
  }

  editPass(clicked_index) {
    this.nativeStorage.getItem('user_profile').then(data => {

      let get_user_name_native = data[clicked_index].name;
      let get_user_id_native = data[clicked_index].userid;
      let get_pass_native = data[clicked_index].pass;
      let type = '1';

      let alert = this.alertCtrl.create({
        title: '<center>Change Status</center>',
        subTitle: 'Name (ID): ' + get_user_name_native + ' (' + get_user_id_native + ') <br><br> New Password:',
        enableBackdropDismiss: false,
        inputs: [
          {
            name: 'password',
            type: 'password',
            placeholder: 'password'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'OK',
            handler: data => {
              this.updateProfile(clicked_index, type, data.password);
              this.loading = this.loadingCtrl.create({
                content: 'Please wait...',
                dismissOnPageChange: true
              });
              this.loading.present();

              setTimeout(() => {
                this.userProfileLoad();
                this.loading.dismiss();
              }, 4000);
            }
          }
        ]
      });
      alert.present();
    })

  }

  editStatus(clicked_index) {
    this.nativeStorage.getItem('user_profile').then(data => {

      let get_user_name_native = data[clicked_index].name;
      let get_user_id_native = data[clicked_index].userid;
      let get_user_status_native = data[clicked_index].status;
      let type = '2';

      let alert = this.alertCtrl.create({
        title: 'Change Status Warning',
        subTitle: 'Are you sure to change the status for ID: ' + get_user_id_native + ' ?',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'OK',
            handler: data => {
              this.updateProfile(clicked_index, type, get_user_status_native);
              this.loading = this.loadingCtrl.create({
                content: 'Please wait...',
                dismissOnPageChange: true
              });
              this.loading.present();

              setTimeout(() => {
                this.userProfileLoad();
                this.loading.dismiss();
              }, 4000);
            }
          }
        ]
      });

      alert.present();
    })
  }

  updateProfile(clicked_index, type, value) {
    this.user_profile = [];
    this.nativeStorage.getItem('user_profile').then(getUserProfile => {
      for (let i = 0; i < getUserProfile.length; i++) {
        if (type == 1) {
          getUserProfile[clicked_index].pass = value;
        }
        else if (type == 2) {
          if (value == 0) {
            getUserProfile[clicked_index].status = "1";
          }
          else if (value == 1) {
            getUserProfile[clicked_index].status = "0";
          }
        }
        this.user_profile.push({ name: getUserProfile[i].name, userid: getUserProfile[i].userid, pass: getUserProfile[i].pass, level: getUserProfile[i].level, status: getUserProfile[i].status, index: getUserProfile[i].index });
      }
      console.log(this.user_profile);
      this.nativeStorage.setItem('user_profile', this.user_profile);
    })

  }

  onChange(selectedValue: any) {
    if (selectedValue == 1) {
      //document.getElementById("table_create_title").style.display = "block";
      //document.getElementById("table_view_edit_title").style.display = "none";
      document.getElementById("create_user").style.display = "block";
      document.getElementById("user_table_remark").style.display = "none";
      document.getElementById("user_table").style.display = "none";
    }
    else if (selectedValue == 2) {
      //document.getElementById("table_create_title").style.display = "none";
      //document.getElementById("table_view_edit_title").style.display = "block";
      document.getElementById("create_user").style.display = "none";
      document.getElementById("user_table_remark").style.display = "block";
      document.getElementById("user_table").style.display = "block";
    }
  }
}
