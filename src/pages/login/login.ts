import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, IonicPage } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
import { NativeStorage } from '@ionic-native/native-storage';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { StatusCheckProvider } from '../../providers/status-check/status-check';
import { Insomnia } from '@ionic-native/insomnia';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: Loading;
  registerCredentials = { id: '', password: '' };
  public statusLoop;

  constructor(private insomnia: Insomnia,private statusCheck: StatusCheckProvider,  private nativeStorage: NativeStorage, public requestService: RequestServiceProvider, private nav: NavController, private auth: AuthService, private alertCtrl: AlertController, private loadingCtrl: LoadingController) { }

  ionViewDidLoad() {
    console.log('check status')
    this.statusLoop = setInterval(() => this.statusCheck.checkStatusLoop(), 1000);  //1 seconds
  }

  public changeSetting() {
    this.nav.push('SettingPage');
  }

  public login() {
    //this.showLoading();
    this.auth.login(this.registerCredentials).subscribe(allowed => {
      if (allowed == 1) {
        this.requestService.requestMachineStatus().then(data => {
          var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
          var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
          if (dev_id_1 == 1000 && dev_id_2 == 1000) {
            this.getLoginUserDetails(this.registerCredentials.id);
          }
          else {
            this.requestService.resetOperation();
            this.getLoginUserDetails(this.registerCredentials.id);
            this.loading.dismiss();
            /*
            this.loading = this.loadingCtrl.create({
              content: 'Please wait...',
              dismissOnPageChange: true
            });
            this.loading.present();

            setTimeout(() => {
              this.requestService.resetOperation();
              this.getLoginUserDetails(this.registerCredentials.id);
              this.loading.dismiss();
            }, 8000);
            */
          }
        })

      }
      else if (allowed == 0) {
        (<HTMLInputElement>document.getElementById("err")).innerHTML = "Incorrect ID/ Pass, Please Try Again.";
        //this.showError("Incorrect ID/ Pass, Please Try Again.");
      }
      else if (allowed == 2) {
        (<HTMLInputElement>document.getElementById("err")).innerHTML = "Disabled To Login, Please Contact Admin.";
        //this.showError("Disabled To Login, Please Contact Admin.");
      }
    },
      error => {
        this.showError(error);
      });
  }

  getLoginUserDetails(id) {
    this.nativeStorage.getItem('user_profile').then(data => {
      let user_count = data.length;
      let loginName;
      let loginLevel;
      let get_user_name_native;
      let get_user_id_native;
      let get_pass_native;
      let get_user_status_native;
      let get_user_level_native;

      for (let i = 0; i < user_count; i++) {
        get_user_name_native = data[i].name;
        get_user_id_native = data[i].userid;
        get_pass_native = data[i].pass;
        get_user_level_native = data[i].level;

        if (get_user_id_native == id) {
          loginName = get_user_name_native;
          loginLevel = get_user_level_native;
        }
      }
      this.nativeStorage.setItem('Login_Info', { ID: this.registerCredentials.id, seqNo: "POS", sessionID: this.registerCredentials.id, name: loginName, level: loginLevel });
      this.insomnia.keepAwake();
      window.localStorage.removeItem('status');
      this.nav.setRoot('HomePage');
    })
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    let alert = this.alertCtrl.create({
      title: 'Alert',
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          role: 'dismiss'
        }
      ]
    });
    alert.present();
  }
}