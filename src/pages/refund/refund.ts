import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { NativeStorage } from '@ionic-native/native-storage';
import { HomePage } from '../home/home';
import { File } from '@ionic-native/file';

@IonicPage()
@Component({
  selector: 'page-refund',
  templateUrl: 'refund.html',
})
export class RefundPage {
  private isDisabled: boolean = true;
  dispenseByAmountForm: FormGroup;
  amount = { "get_amount": "", "total_amount": '' };
  public u: any[] = [];
  public u1: any[] = [];
  public v: any[] = [];
  public v1: any[] = [];
  public checkDisable;
  public storeArray: any = [];
  public checkAmount_Piece_recycler: any[] = [];
  public dispenseDetail: any = [];
  public dispenseFailed = 0;
  public allowToWithdrawId: any = [];
  public allowToWithdrawPiece: any = [];
  public dispenseByAmountArray: any = [];
  public negativeNote: any[] = [];
  public test: any = [];
  public u2: number = 0;
  public dispense10 = 0;
  public dispense5 = 0;
  public dispense2 = 0;
  public dispense1 = 0;
  public dispense050 = 0;
  public dispense020 = 0;
  public dispense010 = 0;
  public dispense005 = 0;
  public total_cash: number;
  public total_sgd1: number;
  public total_coin: number;
  public total_sgd01: number;
  public total_sgd005: number;
  public total_coin_02_01_005: number;
  public total_coin_01_005: number;
  public dispenseAmountDisplay;
  loading: Loading;
  public loadingIsDisplay = 0;
  public statusLoop;
  public dispenseFailed_maxCoin = 0;
  public current_inventory: any[] = [];
  public updateInventoryCall = 0;
  public total_dispense = 0;
  public setRootCall = 0;
  public global_abcde;
  public status = false;

  constructor(private nativeStorage: NativeStorage, private file: File, private loadingCtrl: LoadingController, private nav: NavController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public requestService: RequestServiceProvider) {
  }

  ionViewDidLoad() {
    this.checkInventory();
  }

  ngOnInit() {
    this.dispenseByAmountForm = new FormGroup({
      get_amount: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{1,3}([.][0-9]{1}[05]{1})?$')]),
      total_amount: new FormControl('', [Validators.required])
    });
  }

  public checkInventory() {
    (<HTMLInputElement>document.getElementById("dispense")).disabled = false;
    this.storeArray = [];
    this.dispenseDetail = [];
    this.checkAmount_Piece_recycler = [];
    this.u2 = 0;
    this.requestService.inventoryOperation(2)
      .then(data => {
        let type_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].$["n:type"]; //4(Recycler)
        let body_recycler = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];

        for (let j = 0; j < body_recycler.length; j++) {
          let denominationType_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
          let denominationPiece_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
          let denominationDevid_4 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:devid"];

          let totalCount = ((denominationType_4) / 100) * denominationPiece_4;
          this.v[j] = ((denominationType_4) / 100).toFixed(2);
          this.v1[j] = denominationPiece_4;
          this.u2 += totalCount;
          this.checkDisable = true;

          if (this.v1[j] != 0) {
            this.checkDisable = false;
            //let s = String(j)
            //this.dispenseDetail.push({id:j,value:0});
            this.storeArray.push({ id: j, type: denominationType_4, devid: denominationDevid_4 });
          }
          this.dispenseDetail.push({ id: j, value: 0, disabled: this.checkDisable });
          this.checkAmount_Piece_recycler.push({ amount: this.v[j], piece: this.v1[j], id: String(j), isDisabled: this.checkDisable, devid: denominationDevid_4 });
        }
        console.log(this.checkAmount_Piece_recycler);
        this.amount.total_amount = String((this.u2).toFixed(2));
      });
  }

  public dispenseByAmount() {
    (<HTMLInputElement>document.getElementById("dispense")).disabled = true;
    this.dispenseByAmountArray = [];
    let dispenseAmount = this.amount.get_amount;//50.50
    let current_2 = 0;
    let current_5 = 0;
    let current_10 = 0;
    let current_010 = 0;
    let current_005 = 0;
    let current_1 = 0;
    let current_050 = 0;
    let current_020 = 0;
    let n2 = 0, n5 = 0, n10 = 0, c1 = 0, c010 = 0, c005 = 0, c050 = 0, c020 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "2.00") {
        current_2 = this.checkAmount_Piece_recycler[z].piece;
        n2 = current_2 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "5.00") {
        current_5 = this.checkAmount_Piece_recycler[z].piece;
        n5 = current_5 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "10.00") {
        current_10 = this.checkAmount_Piece_recycler[z].piece;
        n10 = current_10 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "0.10") {
        current_010 = this.checkAmount_Piece_recycler[z].piece;
        c010 = current_010 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "0.05") {
        current_005 = this.checkAmount_Piece_recycler[z].piece;
        c005 = current_005 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "1.00") {
        current_1 = this.checkAmount_Piece_recycler[z].piece;
        c1 = current_1 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "0.50") {
        current_050 = this.checkAmount_Piece_recycler[z].piece;
        c050 = current_050 * this.checkAmount_Piece_recycler[z].amount;
      }
      if (this.checkAmount_Piece_recycler[z].amount == "0.20") {
        current_020 = this.checkAmount_Piece_recycler[z].piece;
        c020 = current_020 * this.checkAmount_Piece_recycler[z].amount;
      }
    }

    this.total_cash = Number(n2) + Number(n5) + Number(n10);
    this.total_sgd1 = Number(c1);
    this.total_coin = Number(c1) + Number(c010) + Number(c005) + Number(c050) + Number(c020);
    this.total_sgd01 = Number(c010);
    this.total_sgd005 = Number(c005);
    this.total_coin_02_01_005 = Number(c010) + Number(c005) + Number(c020);
    this.total_coin_01_005 = Number(c010) + Number(c005);

    if (Number(dispenseAmount) > Number(this.amount.total_amount)) {
      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: "Change Shortage",     //prompt message
        buttons: [
          {
            text: 'OK',
            role: 'dismiss'
          }
        ]
      });
      alert.present();
    }
    else {
      console.log(dispenseAmount);
      let checkDecimal = dispenseAmount.indexOf('.00');
      let new_dispenseAmount = dispenseAmount;
      let dispenseAmount_check1 = Number(dispenseAmount);

      let dispenseAmount_check2 = Number((dispenseAmount_check1 - Math.floor(dispenseAmount_check1)).toFixed(2)); //0.00
      if (dispenseAmount_check2 == 0 && checkDecimal == -1) {
        new_dispenseAmount = dispenseAmount + ".00";  //1.00
        let test = Number(dispenseAmount);
        this.dispenseAmountDisplay = String(test) + '.00';
      }
      else {
        new_dispenseAmount = dispenseAmount;
        this.dispenseAmountDisplay = Number(dispenseAmount);
      }

      let lastTwoDigit = new_dispenseAmount.slice(-2);//50
      let a = Number(lastTwoDigit) / 100;
      if (a > this.total_coin) {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: "Coin Not Enough.",     //prompt message
          buttons: [
            {
              text: 'OK',
              role: 'dismiss'
            }
          ]
        });
        alert.present();
      }

      let doubleDispenseAmount = Number(new_dispenseAmount) * 100;//5050
      let integerOfAmount = doubleDispenseAmount - Number(lastTwoDigit);//5000
      let b = integerOfAmount / 100;
      let failed = 0;

      if (b % 2 == 1 && this.total_sgd1 == 0) {
        if (this.total_coin < 1) {
          let alert = this.alertCtrl.create({
            title: 'Alert',
            subTitle: "SGD 1.00 Not Enough.",     //prompt message
            buttons: [
              {
                text: 'OK',
                role: 'dismiss'
              }
            ]
          });
          alert.present();
          failed = 1;
        }

      }

      if ((b > this.total_cash) && (b > this.total_coin)) {
        let alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: "Change Shortage.",     //prompt message
          buttons: [
            {
              text: 'OK',
              role: 'dismiss'
            }
          ]
        });
        alert.present();
        failed = 1;
      }

      if (failed == 0) {
        this.startDevideAmount(new_dispenseAmount);

        for (let j = 0; j < this.checkAmount_Piece_recycler.length; j++) {

          if (this.checkAmount_Piece_recycler[j].amount == "2.00") {
            this.v1[j] = this.dispense2;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "5.00") {
            this.v1[j] = this.dispense5;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "10.00") {
            this.v1[j] = this.dispense10;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "1.00") {
            this.v1[j] = this.dispense1;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "0.10") {
            this.v1[j] = this.dispense010;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "0.05") {
            this.v1[j] = this.dispense005;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "0.50") {
            this.v1[j] = this.dispense050;
          }
          if (this.checkAmount_Piece_recycler[j].amount == "0.20") {
            this.v1[j] = this.dispense020;
          }
          this.total_dispense += (this.checkAmount_Piece_recycler[j].amount * this.v1[j]);
          this.dispenseByAmountArray.push({ type: String(this.checkAmount_Piece_recycler[j].amount * 100), piece: String(this.v1[j]), devid: this.checkAmount_Piece_recycler[j].devid });
        }
        //console.log(this.dispenseByAmountArray);

        let test = this.dispense020 + this.dispense050 + this.dispense005 + this.dispense1 + this.dispense010;

        console.log(test);
        if (test > 5) {
          console.log('Need to dispense more than 5 piece of coins');
          this.dispenseFailed = 1;
          this.dispenseFailed_maxCoin = 1;
          this.startWithdrawByAmount();
        }
        else {
          this.startWithdrawByAmount();
        }


      }

    }
  }

  startDevideAmount(integerOfAmount) {
    let dispenseAmount = integerOfAmount;//50.50
    let normalDispenseAmount = integerOfAmount;//150
    let checkDispenseType_2 = normalDispenseAmount / 2;// 25
    let checkDispenseType_5 = normalDispenseAmount / 5; //10
    let checkDispenseType_10 = normalDispenseAmount / 10; //5
    let checkDispenseType_100 = normalDispenseAmount / 100; //0.5

    if (checkDispenseType_100 < 1) {

      if (checkDispenseType_10 < 1) {

        if (checkDispenseType_5 < 1) {

          if (checkDispenseType_2 = 0) {
            console.log('Input is 0');
          }
          else if (checkDispenseType_2 > 1 && checkDispenseType_2 < 2) {
            this.math1(checkDispenseType_2);
          }
          else if (checkDispenseType_2 > 0 && checkDispenseType_2 < 1) {
            if (checkDispenseType_2 >= 0.5)
              this.math050(checkDispenseType_2);
            else if (checkDispenseType_2 >= 0.2 && checkDispenseType_2 < 0.5)
              this.math020(checkDispenseType_2);
            else if (checkDispenseType_2 >= 0.1 && checkDispenseType_2 < 0.2)
              this.math010(checkDispenseType_2);
            else if (checkDispenseType_2 >= 0.05 && checkDispenseType_2 < 0.1) {
              this.math005(checkDispenseType_2);
            }
          }
          else {
            this.math2(dispenseAmount);
          }

        }
        else
          this.math5(dispenseAmount);
      }
      else
        this.math10(dispenseAmount);
    }
    else
      this.math100(dispenseAmount);
  }

  math100(amount) {
    let existingSGD10 = 0;
    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "10.00") {    //SGD 10
        existingSGD10 = this.checkAmount_Piece_recycler[z].piece;
      }
    }

    let calculate = (amount / 100).toFixed(4);            //2.51
    let getAmount = Math.floor(Number(calculate));        //2
    let pieceToDispense = getAmount * 10;                     //20 piece of SGD10
    let checkEnough = existingSGD10 - pieceToDispense;    //1-20 = -19
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));
    let getRemaining = Number((Number(getDecimal) * 100).toFixed(2)); //51

    if (checkEnough < 0) {
      getRemaining = (Math.abs(checkEnough) * 10) + getRemaining;     //190+51 = 241
      this.dispense10 = pieceToDispense - Math.abs(checkEnough);        //1
    }
    else
      this.dispense10 = pieceToDispense;  // 20

    console.log('SGD 10 (1)')
    console.log('SGD 10, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD10);
    console.log('To dispense: ' + this.dispense10)

    if (getRemaining >= 10) {
      this.math10(getRemaining);
    }
    else if (getRemaining >= 5 && getRemaining < 10) {
      this.math5(getRemaining);
    }
    else if (getRemaining >= 2 && getRemaining < 5) {
      this.math2(getRemaining);
    }
    else if (getRemaining >= 1 && getRemaining < 2) {
      this.math1(getRemaining);
    }
    else if (getRemaining > 0 && getRemaining < 1) {
      if (getRemaining >= 0.5)
        this.math050(getRemaining);
      else if (getRemaining >= 0.2 && getRemaining < 0.5)
        this.math020(getRemaining);
      else if (getRemaining >= 0.1 && getRemaining < 0.2)
        this.math010(getRemaining);
      else if (getRemaining >= 0.05 && getRemaining < 0.1) {
        this.math005(getRemaining);
      }
    }
  }

  math10(amount) {
    console.log(this.checkAmount_Piece_recycler);
    let existingSGD10 = 0;
    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "10.00") {
        existingSGD10 = this.checkAmount_Piece_recycler[z].piece - this.dispense10;
      }
    }

    let calculate = (amount / 10).toFixed(4);   //10.60
    let getAmount = Math.floor(Number(calculate));  //10
    let pieceToDispense = getAmount;               //10
    let checkEnough = existingSGD10 - pieceToDispense;  //-9
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));    //0.60
    let getRemaining = Number((Number(getDecimal) * 10).toFixed(2)); //6.0

    if (checkEnough < 0) {
      getRemaining = (Math.abs(checkEnough) * 10) + getRemaining;
      this.dispense10 = this.dispense10 + (pieceToDispense - Math.abs(checkEnough));
    }
    else
      this.dispense10 = this.dispense10 + pieceToDispense;  // 20

    console.log('SGD 10');
    console.log('SGD 10, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD10);
    console.log('To dispense: ' + this.dispense10)

    if (getRemaining >= 5) {
      this.math5(getRemaining);
    }
    else if (getRemaining >= 2 && getRemaining < 5) {
      this.math2(getRemaining);
    }
    else if (getRemaining >= 1 && getRemaining < 2) {
      this.math1(getRemaining);
    }
    else if (getRemaining > 0 && getRemaining < 1) {
      if (getRemaining >= 0.5)
        this.math050(getRemaining);
      else if (getRemaining >= 0.2 && getRemaining < 0.5)
        this.math020(getRemaining);
      else if (getRemaining >= 0.1 && getRemaining < 0.2)
        this.math010(getRemaining);
      else if (getRemaining >= 0.05 && getRemaining < 0.1) {
        this.math005(getRemaining);
      }
    }
  }

  math5(amount) {//5.5
    let existingSGD5 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "5.00") {
        existingSGD5 = this.checkAmount_Piece_recycler[z].piece;
      }
    }

    let calculate = (amount / 5).toFixed(4);
    let getAmount = Math.floor(Number(calculate));
    let pieceToDispense = getAmount;
    let checkEnough = existingSGD5 - pieceToDispense;
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));
    let getRemaining = Number((Number(getDecimal) * 5).toFixed(2)); //8

    if (checkEnough < 0) {
      getRemaining = (Math.abs(checkEnough) * 5) + getRemaining;
      this.dispense5 = pieceToDispense - Math.abs(checkEnough);
    }
    else
      this.dispense5 = pieceToDispense;  // 20

    console.log('SGD 5');
    console.log('SGD 5, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD5);
    console.log('To dispense: ' + this.dispense5);


    let checking = getRemaining - Number((Number(getDecimal) * 5).toFixed(2));
    let a = checking % 2;
    let b = 1 + Number((Number(getDecimal) * 5).toFixed(2));

    if (a == 1 && this.total_sgd1 < 1 && this.total_coin < b) {
      if (this.dispense5 != 0) {
        this.dispense5 = this.dispense5 - 1;
        getRemaining = Number(getRemaining) + 5;
      }
      else {
        console.log("cannot dispense");
        this.dispenseFailed = 1;
      }
    }


    if (getRemaining >= 2) {
      this.math2(getRemaining);
    }
    else if (getRemaining >= 1 && getRemaining < 2) {
      this.math1(getRemaining);
    }
    else if (getRemaining > 0 && getRemaining < 1) {
      if (getRemaining >= 0.5)
        this.math050(getRemaining);
      else if (getRemaining >= 0.2 && getRemaining < 0.5)
        this.math020(getRemaining);
      else if (getRemaining >= 0.1 && getRemaining < 0.2)
        this.math010(getRemaining);
      else if (getRemaining >= 0.05 && getRemaining < 0.1) {
        this.math005(getRemaining);
      }
    }
  }

  math2(amount) {                           //3.5
    let existingSGD2 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "2.00") {
        existingSGD2 = this.checkAmount_Piece_recycler[z].piece;
      }
    }

    let calculate = (amount / 2).toFixed(4);
    let getAmount = Math.floor(Number(calculate));
    let pieceToDispense = getAmount;
    let checkEnough = existingSGD2 - pieceToDispense;
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));
    let getRemaining = Number((Number(getDecimal) * 2).toFixed(2)); //8

    if (checkEnough < 0) {
      getRemaining = (Math.abs(checkEnough) * 2) + getRemaining;
      this.dispense2 = pieceToDispense - Math.abs(checkEnough);
    }
    else
      this.dispense2 = pieceToDispense;  // 20

    console.log('SGD 2');
    console.log('SGD 2, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD2);
    console.log('To dispense: ' + this.dispense2);


    if (this.total_coin < getRemaining) {
      console.log("cannot dispense");
      this.dispenseFailed = 1;
    }
    else {
      if (getRemaining >= 1) {
        this.math1(getRemaining);
      }
      else if (getRemaining < 1 && getRemaining > 0) {
        if (getRemaining >= 0.5)
          this.math050(getRemaining);
        else if (getRemaining >= 0.2 && getRemaining < 0.5)
          this.math020(getRemaining);
        else if (getRemaining >= 0.1 && getRemaining < 0.2)
          this.math010(getRemaining);
        else if (getRemaining >= 0.05 && getRemaining < 0.1) {
          this.math005(getRemaining);
        }
      }
    }
  }

  math1(amount) {
    let existingSGD1 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "1.00") {
        existingSGD1 = this.checkAmount_Piece_recycler[z].piece;
      }
    }

    let getAmount = Math.floor(Number(amount));
    let pieceToDispense = getAmount;
    let checkEnough = existingSGD1 - pieceToDispense;
    let getDecimal = Number((Number(amount) - getAmount).toFixed(4));
    let getRemaining = Number(Number(getDecimal).toFixed(2)); //8

    if (checkEnough < 0) {
      getRemaining = Math.abs(checkEnough) + getRemaining;
      this.dispense1 = pieceToDispense - Math.abs(checkEnough);
    }
    else
      this.dispense1 = pieceToDispense;  // 20

    console.log('SGD 1');
    console.log('SGD 1, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD1);
    console.log('To dispense: ' + this.dispense1)


    if (getRemaining >= 0.5)
      this.math050(getRemaining);
    else if (getRemaining >= 0.2 && getRemaining < 0.5)
      this.math020(getRemaining);
    else if (getRemaining >= 0.1 && getRemaining < 0.2)
      this.math010(getRemaining);
    else if (getRemaining >= 0.05 && getRemaining < 0.1) {
      this.math005(getRemaining);
    }

  }

  math050(amount) {
    let existingSGD050 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "0.50") {
        existingSGD050 = this.checkAmount_Piece_recycler[z].piece;
      }
    }

    let calculate = (amount / 0.5).toFixed(4);
    let getAmount = Math.floor(Number(calculate));
    let pieceToDispense = getAmount;
    let checkEnough = existingSGD050 - pieceToDispense;
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));
    let getRemaining = Number((Number(getDecimal) * 0.5).toFixed(2)); //8

    if (checkEnough < 0) {
      getRemaining = Math.abs(checkEnough) + getRemaining;
      this.dispense050 = pieceToDispense - Math.abs(checkEnough);
    }
    else
      this.dispense050 = pieceToDispense;  // 20

    console.log('SGD 0.5');
    console.log('SGD 0.5, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD050);
    console.log('To dispense: ' + this.dispense050);


    if (this.total_coin_02_01_005 < getRemaining) {
      this.dispenseFailed = 1;
    }
    else {
      let checking = getRemaining.toFixed(2);
      let checking2 = checking.slice(-1);
      let checking3;
      if (Number(checking2) == 5) {
        checking3 = Number(checking) - 0.05;
      }
      else {
        checking3 = Number(checking);
      }

      let a = (checking3 * 10) % 2;

      if (a == 1 && this.total_sgd01 < 1 && this.total_sgd005 < 2) {
        if (this.dispense050 != 0) {
          this.dispense050 = this.dispense050 - 1;
          getRemaining = Number(getRemaining) + 0.5;
        }
        else {
          console.log("cannot dispense");
          this.dispenseFailed = 1;
        }
      }
    }


    if (getRemaining >= 0.2)
      this.math020(getRemaining);
    else if (getRemaining >= 0.1 && getRemaining < 0.2)
      this.math010(getRemaining);
    else if (getRemaining >= 0.05 && getRemaining < 0.1) {
      this.math005(getRemaining);
    }
  }

  math020(amount) {
    let existingSGD020 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "0.20") {
        existingSGD020 = this.checkAmount_Piece_recycler[z].piece;
      }
    }

    let calculate = (amount / 0.2).toFixed(4);
    let getAmount = Math.floor(Number(calculate));
    let pieceToDispense = getAmount;
    let checkEnough = existingSGD020 - pieceToDispense;
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));
    let getRemaining = Number((Number(getDecimal) * 0.2).toFixed(2)); //8

    console.log('SGD 0.2');
    console.log('SGD 0.2, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD020);
    console.log('To dispense: ' + this.dispense020);


    if (checkEnough < 0) {
      getRemaining = Math.abs(checkEnough) + getRemaining;
      this.dispense020 = pieceToDispense - Math.abs(checkEnough);
    }
    else
      this.dispense020 = pieceToDispense;  // 20

    if (this.total_coin_01_005 < getRemaining) {
      console.log("cannot dispense");
      this.dispenseFailed = 1;
    }
    else {
      if (getRemaining >= 0.1)
        this.math010(getRemaining);
      else if (getRemaining >= 0.05 && getRemaining < 0.1) {
        this.math005(getRemaining);
      }
    }

  }

  math010(amount) {                           //0.1
    let existingSGD010 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "0.10") {
        existingSGD010 = this.checkAmount_Piece_recycler[z].piece;
      }
    }
    let calculate = (amount / 0.1).toFixed(4);
    let getAmount = Math.floor(Number(calculate));
    let pieceToDispense = getAmount;
    let checkEnough = existingSGD010 - pieceToDispense;
    let getDecimal = Number((Number(calculate) - getAmount).toFixed(4));
    let getRemaining = Number((Number(getDecimal) * 0.1).toFixed(2)); //8

    if (checkEnough < 0) {
      getRemaining = Math.abs(checkEnough) + getRemaining;
      this.dispense010 = pieceToDispense - Math.abs(checkEnough);
    }
    else
      this.dispense010 = pieceToDispense;  // 20

    console.log('SGD 0.1');
    console.log('SGD 0.1, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD010);
    console.log('To dispense: ' + this.dispense010);

    if (getRemaining != 0) {
      this.math005(getRemaining);
    }
  }

  math005(amount) {
    let existingSGD005 = 0;

    for (let z = 0; z < this.checkAmount_Piece_recycler.length; z++) {
      if (this.checkAmount_Piece_recycler[z].amount == "0.05") {
        existingSGD005 = this.checkAmount_Piece_recycler[z].piece;
      }
    }
    let calculate = (amount / 0.05).toFixed(4);//1
    let getAmount = Math.floor(Number(calculate));//1
    let pieceToDispense = getAmount;//1
    let checkEnough = existingSGD005 - pieceToDispense;//-1
    let getDecimal = Number((Number(calculate) - existingSGD005).toFixed(4));//0
    let getRemaining = Number((Number(getDecimal) * 0.05).toFixed(2));
    this.dispense005 = pieceToDispense;

    console.log('SGD 0.05');
    console.log('SGD 0.05, piece: ' + getAmount);
    console.log('Enough?: ' + checkEnough);
    console.log('Remain: ' + getRemaining);
    console.log('Existing: ' + existingSGD005);
    console.log('To dispense: ' + this.dispense005);

    if (Number(checkEnough) < 0) {
      console.log("cannot dispense");
      this.dispenseFailed = 1;
    }
  }


  startWithdrawByAmount() {
    let failedMsg;
    if (this.dispenseFailed == 1) {
      if (this.dispenseFailed_maxCoin == 1) {
        failedMsg = 'Failed to dispense. Reached maximum amount of dispensable coin.'
      }
      else if (this.dispenseFailed_maxCoin == 0) {
        failedMsg = 'Failed to dispense. SGD 1.00 or SGD 0.10 or SGD 0.05 Not Enough.'
      }

      let alert = this.alertCtrl.create({
        title: 'Alert',
        subTitle: failedMsg,     //prompt message
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.amount.get_amount = '';
              this.dispenseByAmountArray = [];
            }
          }
        ]
      });
      alert.present();
    }
    else {
      console.log(this.dispenseByAmountArray);
      this.requestService.cashoutOperation(this.dispenseByAmountArray).then(data => {
        var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:CashoutResponse"]["0"].$["n:result"];
        this.transactionSaveStorage(statusCode);
        var status_abc = setInterval(() => {
          this.checkStatusLoop();
          if (this.status == true) {
            clearInterval(status_abc);
            this.updateLastestInventory();
            document.getElementById('status').innerHTML = "Last Dispense Amount: ";
            document.getElementById('statusSales').innerHTML = "SGD " + this.dispenseAmountDisplay;
            this.amount.get_amount = '';
            this.dispenseByAmountArray = [];
          }
        }, 600);
      });
    }
  }


  public checkStatusLoop() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      //var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];

      if ((dev_id_1 == 1000 && dev_id_2 == 1000)) {
        this.status = true;
      }
      else {
        this.status = false;
      }
    })
  }



  transactionSaveStorage(statusCode) {
    var today = new Date();
    var yy = today.getFullYear();
    var dd = ("0" + today.getDate()).slice(-2);
    var mm = ("0" + (today.getMonth() + 1)).slice(-2);
    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();

    let currentIndex = 0;
    let dateTime = dd + '_' + mm + '_' + hh + '_' + min + '_' + ss;
    let fileName = dateTime;
    let storageFileName = yy + '-' + mm + '-' + dd;;
    let date = dd + '/' + mm + '/' + yy;
    let time = hh + ':' + min + ':' + ss;
    let msg = '';
    if (Number(statusCode) == 0) {
      msg = 'Success';
    }
    else if (Number(statusCode) == 10) {
      msg = 'Change Shortage, No Amount Dispensed.'
    }
    else if (Number(statusCode) == 11) {
      msg = 'Device Error'
    }
    else
      msg = statusCode;

    //let test = [{index: 0, id: fileName, date: date, time: time, sales: this.amount.get_amount, insert_amount: this.amount.cash, change: this.amount.change}]
    //this.nativeStorage.setItem('transactionDetails', test);


    this.nativeStorage.getItem(storageFileName + 'operationDetails').then(data => {
      this.storeArray = [];
      let transactionDetails_length = Number(data.length);
      for (let i = 0; i < transactionDetails_length; i++) {

        this.storeArray.push({ index: i, operation: data[i].operation, date: data[i].date, time: data[i].time, sales: data[i].sales, deposit_details: data[i].deposit_details, total_insert_amount: data[i].total_insert_amount, change: data[i].change, status: data[i].status });
        currentIndex = i + 1;
      }
    }, err => {
      console.log('not existing');
    })

    setTimeout(() => {
      this.storeArray.push({ index: currentIndex, operation: "Refund", date: date, time: time, sales: '', deposit_details: '', total_insert_amount: '', change: (this.total_dispense).toFixed(2), status: msg });
      this.nativeStorage.setItem(storageFileName + 'operationDetails', this.storeArray);
    }, 1500);


    this.file.checkDir(this.file.externalDataDirectory, 'dispense').
      then(_ => {
        console.log('Directory exists');
        this.file.writeFile(
          this.file.externalDataDirectory + '/dispense',
          fileName + '.txt',
          'Refund Information\n' +
          '==================================\n' +
          'Status: ' + msg + '\n' +
          'Date: ' + date + '\n' +
          'Time: ' + time + '\n' +
          'Refund Amount (SGD): ' + (this.total_dispense).toFixed(2) + '\n',
          { replace: false }).
          then(_ => console.log('Success')).
          catch(err => console.log('Failed'));
      }
      ).
      catch(
        err => {
          console.log('Directory doesn\'t exist');
          this.file.createDir(this.file.externalDataDirectory, 'dispense', false);
          this.file.writeFile(
            this.file.externalDataDirectory + '/dispense',
            fileName + '.txt',
            'Sales Information\n' +
            '==================================\n' +
            'Date: ' + date + '\n' +
            'Time: ' + time + '\n' +
            'Refund Amount (SGD): ' + (this.total_dispense).toFixed(2) + '\n' +
            'Status: ' + msg + '\n',
            { replace: false }).
            then(_ => console.log('Success')).
            catch(err => console.log('Failed'));
        }
      );
  }


  updateLastestInventory() {
    this.updateInventoryCall = 1;
    this.loading = this.loadingCtrl.create({
      content: 'Updating Inventory...'
    });

    this.loading.present();

    this.current_inventory = [];

    var abc = setInterval(() => {
      this.testingb();
      if (this.global_abcde == true) {
        clearInterval(abc);
        this.testingc();
      }
    }, 500);

  }


  testingb() {
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      if (dev_id_1 == '1000' && dev_id_2 == '1000') {
        this.global_abcde = true;
      }
      else {
        this.global_abcde = false;
      }
    });
  }


  testingc() {
    console.log('testing c');
    var abcde = setInterval(() => {
      this.testingd();
      if (this.testingd() == true) {
        clearInterval(abcde);
        this.testinge();
      }
    }, 1500);

  }

  testingd() {
    var sts = true;
    this.requestService.inventoryOperation(2).then(sales => {
      let stts = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].$["n:result"];
      if (stts == '10') {
        sts = true;
      }
      else {
        sts = false;
      }
    });
    return sts;
  }

  testinge() {
    this.requestService.inventoryOperation(2).then(sales => {
      console.log('here');
      console.log(sales);
      let body_recycler = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"]["Denomination"];
      for (let j = 0; j < body_recycler.length; j++) {
        let denominationType_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j].$["n:fv"];
        let denominationPiece_4 = sales["soapenv:Envelope"]["soapenv:Body"]["0"]["n:InventoryResponse"]["0"].Cash["0"].Denomination[j]["n:Piece"]["0"];
        this.v[j] = ((denominationType_4) / 100).toFixed(2);
        this.v1[j] = denominationPiece_4;

        this.current_inventory.push({ amount: this.v[j], piece: this.v1[j] });

        console.log('new current invent');
        console.log(this.current_inventory);

        setTimeout(() => {
          console.log('new current invent2');
          console.log(this.current_inventory);
          this.nativeStorage.setItem('last_inventory', this.current_inventory).then(
            () => {
              console.log('updated lastest inventory');
              if(this.setRootCall == 0){
                this.setRootCall = 1;
                this.nav.setRoot(HomePage);
              }
              this.updateInventoryCall = 0;
              this.loading.dismiss();
            },
            error => console.error('Error storing item', error)
          );
        }, 1400);
      }
    });
  }
}
