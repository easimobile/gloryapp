import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import xml2js from 'xml2js';
import { NativeStorage } from '@ionic-native/native-storage';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Insomnia } from '@ionic-native/insomnia';
import { LoadingController, Loading, AlertController } from 'ionic-angular';

@Injectable()
export class RequestServiceProvider {
  public cashoutRequestStringBody: any = [];
  public pushRequestStringBody: any = [];
  public get_seqNo;
  public get_id;
  public get_sessionID;
  public get_ipAddress;
  public machineIP;
  public alert = 0;
  public loading;
  public loadingIsDisplay = 0;

  constructor(private insomnia: Insomnia, private auth: AuthService, public loadingCtrl: LoadingController, private nativeStorage: NativeStorage, public http: Http, private alertCtrl: AlertController) {
    this.nativeStorage.getItem('Login_Info').then(login_info => {
      this.get_seqNo = login_info.seqNo;
      this.get_id = login_info.ID;
      this.get_sessionID = login_info.sessionID;
    });

    this.nativeStorage.getItem('IPAddress').then(ip => {
      this.get_ipAddress = ip.IP_Address;
      this.machineIP = 'http://' + this.get_ipAddress + '/axis2/services/BrueBoxService';
    });
  }

  requestMachineStatus() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "GetStatus");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:StatusRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="1"/><RequireVerification bru:type="?"/></bru:StatusRequest></soapenv:Body></soapenv:Envelope>'
    //let returnString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header/><soapenv:Body><n:StatusResponse n:result="0" xmlns:n="http://www.glory.co.jp/bruebox.xsd"><n:Id>?</n:Id><n:SeqNo>?</n:SeqNo><n:User/><Status><n:Code>1</n:Code><DevStatus n:devid="1" n:val="0" n:st="1000"/><DevStatus n:devid="2" n:val="0" n:st="9100"/></Status><Cash n:type="1"/></n:StatusResponse></soapenv:Body></soapenv:Envelope>';
    return new Promise(resolve => {
      /*
      xml2js.parseString(returnString, function (err, result) {
        resolve(result);
        });
      */
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            if(this.alert == 1){
               this.loading.dismiss();
            }
           
            this.alert = 0;
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('Server Down');

          if (this.alert == 0) {
            this.alert = 1;
            this.auth.logout().subscribe(succ => {
              window.localStorage.removeItem('expired_session');
              this.insomnia.allowSleepAgain();
              this.nativeStorage.remove('Login_Info');
            });
            this.loading = this.loadingCtrl.create({
              content: 'No Connection ...'
            });

            this.loading.present();
          }

        });
    });
  }

  getCashInAmount() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "GetStatus");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:StatusRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="1"/><Option bru:type="2"/><RequireVerification bru:type="?"/></bru:StatusRequest></soapenv:Body></soapenv:Envelope>'
    let returnString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header></soapenv:Header><soapenv:Body><n:StatusResponse xmlns:n="http://www.glory.co.jp/bruebox.xsd" n:result="0"><n:Id>?</n:Id><n:SeqNo>?</n:SeqNo><n:User></n:User><Status><n:Code>3</n:Code><DevStatus n:devid="1" n:val="0" n:st="2000"></DevStatus><DevStatus n:devid="2" n:val="0" n:st="2000"></DevStatus></Status><Cash n:type="1"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>2</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>2</n:Piece><n:Status>0</n:Status></Denomination></Cash></n:StatusResponse></soapenv:Body></soapenv:Envelope>';

    return new Promise(resolve => {
      /*
       xml2js.parseString(returnString, function (err, result) {
         console.log(result);
         resolve(result);
       });
       */
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  //request to cash in
  startCashInOperation() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "StartCashinOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:StartCashinRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID></bru:StartCashinRequest></soapenv:Body></soapenv:Envelope>'
    //let responseString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"> <soapenv:Header/><soapenv:Body><n:StartCashinResponse n:result="0" xmlns:n="http://www.glory.co.jp/bruebox.xsd"><n:Id>?</n:Id> <n:SeqNo>?</n:SeqNo> <n:User/></n:StartCashinResponse> </soapenv:Body></soapenv:Envelope>';
    return new Promise(resolve => {

      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            //resolve(response);    //pass the response back.
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  //request to cash in
  endCashInOperation() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "EndCashinOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:EndCashinRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID></bru:EndCashinRequest></soapenv:Body></soapenv:Envelope>'
    //let responseString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"> <soapenv:Header/><soapenv:Body><n:StartCashinResponse n:result="0" xmlns:n="http://www.glory.co.jp/bruebox.xsd"><n:Id>?</n:Id> <n:SeqNo>?</n:SeqNo> <n:User/></n:StartCashinResponse> </soapenv:Body></soapenv:Envelope>';
    return new Promise(resolve => {

      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            //resolve(response);    //pass the response back.
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  // calculate the balance
  refreshSalesTotalOperation(sales) {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "RefreshSalesTotalOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:RefreshSalesTotalRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><bru:Amount>' + sales + '</bru:Amount></bru:RefreshSalesTotalRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  //to return the balance to customer, end the process
  changeOperation(sales) {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "ChangeOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:ChangeRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><bru:Amount>' + sales + '</bru:Amount><Cash bru:type="?"><Denomination bru:cc="?" bru:fv="?" bru:rev="?" bru:devid="?"><bru:Piece>?</bru:Piece><bru:Status>?</bru:Status></Denomination></Cash></bru:ChangeRequest></soapenv:Body></soapenv:Envelope>'
    //let responseString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header/><soapenv:Body><n:ChangeResponse n:result="10" xmlns:n="http://www.glory.co.jp/bruebox.xsd"><n:Id>?</n:Id><n:SeqNo>?</n:SeqNo><n:User/><n:Amount>0</n:Amount><n:ManualDeposit>0</n:ManualDeposit><Status><n:Code>19</n:Code><DevStatus n:devid="1" n:val="0" n:st="1500"/><DevStatus n:devid="2" n:val="0" n:st="1500"/></Status><Cash n:type="1"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="500" n:rev="0" n:devid="1"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>2</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10000" n:rev="0" n:devid="1"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>0</n:Status></Denomination></Cash><Cash n:type="2"/></n:ChangeResponse></soapenv:Body></soapenv:Envelope>';

    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        /*
        xml2js.parseString(responseString, function (err, result) {
          //console.log(result);
          resolve(result);
        });
        */
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  //cancel te cash in request
  cashinCancelOperation() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "CashinCancelOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CashinCancelRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID></bru:CashinCancelRequest></soapenv:Body></soapenv:Envelope>'
    //let responseString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"> <soapenv:Header/><soapenv:Body><n:CashinCancelResponse n:result="0" xmlns:n="http://www.glory.co.jp/bruebox.xsd"><n:Id>?</n:Id> <n:SeqNo>?</n:SeqNo> <n:User/></n:CashinCancelResponse> </soapenv:Body></soapenv:Envelope>';

    return new Promise(resolve => {
      /*
      xml2js.parseString(responseString, function (err, result) {
        console.log(result);
        resolve(result);
      });
      */
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  //Start of Dispense Part
  cashoutOperation(dispenseDetails) {
    this.cashoutRequestStringBody = [];
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "CashoutOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestStringHeader = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CashoutRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Delay bru:type="?" bru:time="?"/><Cash bru:type="2">'
    let requestStringTail = '</Cash></bru:CashoutRequest></soapenv:Body></soapenv:Envelope>';
    let bodyLengthForDispenseDetails = dispenseDetails.length;

    for (let i = 0; i < bodyLengthForDispenseDetails; i++) {
      this.cashoutRequestStringBody[i] = '<Denomination bru:cc="SGD" bru:fv="' + dispenseDetails[i].type + '" bru:rev="0" bru:devid="' + dispenseDetails[i].devid + '"><bru:Piece>' + dispenseDetails[i].piece + '</bru:Piece><bru:Status>0</bru:Status></Denomination>';
    }
    let requestString = requestStringHeader + this.cashoutRequestStringBody + requestStringTail

    //let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CashoutRequest><bru:Id>?</bru:Id><bru:SeqNo>?</bru:SeqNo> <Delay bru:type="?" bru:time="?"/><Cash bru:type="?"><Denomination bru:cc="?" bru:fv="?" bru:rev="?" bru:devid="?"><bru:Piece>?</bru:Piece><bru:Status>?</bru:Status></Denomination></Cash></bru:CashoutRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });

  }
  //End of Dispense Part

  //Start of Dispense Part
  cashoutOperationByAmount(dispenseDetails) {
    this.cashoutRequestStringBody = [];
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "CashoutOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestStringHeader = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CashoutRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Delay bru:type="?" bru:time="?"/><Cash bru:type="2">'
    let requestStringTail = '</Cash></bru:CashoutRequest></soapenv:Body></soapenv:Envelope>';
    let bodyLengthForDispenseDetails = dispenseDetails.length;

    for (let i = 0; i < bodyLengthForDispenseDetails; i++) {
      this.cashoutRequestStringBody[i] = '<Denomination bru:cc="SGD" bru:fv="' + dispenseDetails[i].type + '" bru:rev="0" bru:devid="' + dispenseDetails[i].devid + '"><bru:Piece>' + dispenseDetails[i].piece + '</bru:Piece><bru:Status>0</bru:Status></Denomination>';
    }
    let requestString = requestStringHeader + this.cashoutRequestStringBody + requestStringTail

    //let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CashoutRequest><bru:Id>?</bru:Id><bru:SeqNo>?</bru:SeqNo> <Delay bru:type="?" bru:time="?"/><Cash bru:type="?"><Denomination bru:cc="?" bru:fv="?" bru:rev="?" bru:devid="?"><bru:Piece>?</bru:Piece><bru:Status>?</bru:Status></Denomination></Cash></bru:CashoutRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });

  }
  //End of Dispense Part

  //Start of Inventory Part
  inventoryOperation(request_type) {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "InventoryOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:InventoryRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="' + request_type + '"/></bru:InventoryRequest></soapenv:Body></soapenv:Envelope>'
    //let responseString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header/><soapenv:Body><n:InventoryResponse n:result="0" xmlns:n="http://www.glory.co.jp/bruebox.xsd"><n:Id>?</n:Id><n:SeqNo>?</n:SeqNo><n:User/><Cash n:type="3"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="500" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="000" n:fv="0" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>112</n:Piece><n:Status>2</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10" n:rev="0" n:devid="2"><n:Piece>21</n:Piece><n:Status>2</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100" n:rev="0" n:devid="2"><n:Piece>40</n:Piece><n:Status>2</n:Status></Denomination><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>64</n:Piece><n:Status>2</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5" n:rev="0" n:devid="2"><n:Piece>16</n:Piece><n:Status>2</n:Status></Denomination></Cash><Cash n:type="4"><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>1</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>1</n:Status></Denomination></Cash><CashUnits n:devid="1"><CashUnit n:unitno="4043" n:st="0" n:nf="94" n:ne="10" n:max="105"><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4044" n:st="0" n:nf="99" n:ne="11" n:max="111"><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4045" n:st="0" n:nf="106" n:ne="11" n:max="118"><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4056" n:st="0" n:nf="270" n:ne="30" n:max="300"><Denomination n:cc="000" n:fv="0" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4057" n:st="0" n:nf="270" n:ne="30" n:max="300"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="500" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4058" n:st="0" n:nf="270" n:ne="30" n:max="300"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="500" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4059" n:st="0" n:nf="270" n:ne="30" n:max="300"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="500" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4060" n:st="0" n:nf="270" n:ne="30" n:max="300"><Denomination n:cc="SGD" n:fv="200" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="500" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="1000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination><Denomination n:cc="SGD" n:fv="100000" n:rev="0" n:devid="1"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4061" n:st="22" n:nf="0" n:ne="0" n:max="0"/><CashUnit n:unitno="4062" n:st="22" n:nf="0" n:ne="0" n:max="0"/><CashUnit n:unitno="4063" n:st="22" n:nf="0" n:ne="0" n:max="0"/><CashUnit n:unitno="4074" n:st="22" n:nf="0" n:ne="0" n:max="0"/><CashUnit n:unitno="4075" n:st="22" n:nf="0" n:ne="0" n:max="0"/><CashUnit n:unitno="4076" n:st="22" n:nf="0" n:ne="0" n:max="0"/><CashUnit n:unitno="4069" n:st="0" n:nf="0" n:ne="0" n:max="0"/></CashUnits><CashUnits n:devid="2"><CashUnit n:unitno="4043" n:st="0" n:nf="108" n:ne="12" n:max="120"><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4044" n:st="0" n:nf="270" n:ne="30" n:max="300"><Denomination n:cc="SGD" n:fv="10" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4045" n:st="0" n:nf="90" n:ne="10" n:max="100"><Denomination n:cc="SGD" n:fv="100" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4046" n:st="0" n:nf="108" n:ne="12" n:max="120"><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4047" n:st="1" n:nf="180" n:ne="20" n:max="200"><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>1</n:Status></Denomination></CashUnit><CashUnit n:unitno="4048" n:st="0" n:nf="90" n:ne="10" n:max="100"><Denomination n:cc="SGD" n:fv="100" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4054" n:st="1" n:nf="315" n:ne="35" n:max="350"><Denomination n:cc="SGD" n:fv="5" n:rev="0" n:devid="2"><n:Piece>1</n:Piece><n:Status>1</n:Status></Denomination></CashUnit><CashUnit n:unitno="4055" n:st="0" n:nf="180" n:ne="20" n:max="200"><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>0</n:Status></Denomination></CashUnit><CashUnit n:unitno="4084" n:st="4" n:nf="0" n:ne="0" n:max="0"><Denomination n:cc="SGD" n:fv="100" n:rev="0" n:devid="2"><n:Piece>40</n:Piece><n:Status>4</n:Status></Denomination><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>112</n:Piece><n:Status>4</n:Status></Denomination><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>63</n:Piece><n:Status>4</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10" n:rev="0" n:devid="2"><n:Piece>21</n:Piece><n:Status>4</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5" n:rev="0" n:devid="2"><n:Piece>15</n:Piece><n:Status>4</n:Status></Denomination></CashUnit><CashUnit n:unitno="4165" n:st="22" n:nf="0" n:ne="0" n:max="0"><Denomination n:cc="SGD" n:fv="100" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>22</n:Status></Denomination><Denomination n:cc="SGD" n:fv="50" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>22</n:Status></Denomination><Denomination n:cc="SGD" n:fv="20" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>22</n:Status></Denomination><Denomination n:cc="SGD" n:fv="10" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>22</n:Status></Denomination><Denomination n:cc="SGD" n:fv="5" n:rev="0" n:devid="2"><n:Piece>0</n:Piece><n:Status>22</n:Status></Denomination></CashUnit></CashUnits></n:InventoryResponse></soapenv:Body></soapenv:Envelope>'

    return new Promise(resolve => {

      /*
      xml2js.parseString(responseString, function (err, result) {
        console.log(result);
        resolve(result);
      });
      */
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }
  //End of Inventory Part


  //Start of CashBox push
  pushToCashBoxOperation(pushDetails) {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "CollectOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;
    let requestStringHeader = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CollectRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="?"/><Mix bru:type="?"/><IFCassette bru:type="?"/><RequireVerification bru:type="?"/><Partial bru:type="?"/><Cash bru:type="2">'
    let requestStringTail = '</Cash></bru:CollectRequest></soapenv:Body></soapenv:Envelope>';
    let bodyLengthForDispenseDetails = pushDetails.length;

    for (let i = 0; i < bodyLengthForDispenseDetails; i++) {
      this.pushRequestStringBody[i] = '<Denomination bru:cc="SGD" bru:fv="' + pushDetails[i].type + '" bru:rev="0" bru:devid="' + pushDetails[i].devid + '"><bru:Piece>' + pushDetails[i].piece + '</bru:Piece><bru:Status>0</bru:Status></Denomination>';
    }
    let requestString = requestStringHeader + this.pushRequestStringBody + requestStringTail

    // let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:CollectRequest><bru:Id>?</bru:Id><bru:SeqNo>?</bru:SeqNo><bru:SessionID>?</bru:SessionID><Option bru:type="?"/><Mix bru:type="?"/><IFCassette bru:type="?"/><RequireVerification bru:type="?"/><Partial bru:type="?"/><Cash bru:type="?"><Denomination bru:cc="?" bru:fv="?" bru:rev="?" bru:devid="?"><bru:Piece>?</bru:Piece><bru:Status>?</bru:Status></Denomination></Cash></bru:CollectRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }
  //End of CashBox push


  //Start of Lock & Unlock Cashbox Part
  //lock cash box operation
  lockUnitOperation(checkLockType) {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "LockUnitOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:LockUnitRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="' + checkLockType + '"/></bru:LockUnitRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  //unlock cash box operation
  unLockUnitOperation(checkUnlockType) {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "UnLockUnitOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:UnLockUnitRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="' + checkUnlockType + '"/></bru:UnLockUnitRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            //error
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }
  //End of Lock & Unlock Cashbox Part

  //Start of Reset BrueBox Part
  resetOperation() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "ResetOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:ResetRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID></bru:ResetRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }
  //End of Reset BrueBox Part

  //Start of coin return Part
  coinReturnOperation() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "ReturnCashOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;

    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:ReturnCashRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="2"/></bru:ReturnCashRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }
  //End of coin return Part

  //Start of coin return Part
  rebootMachine() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "PowerControlRequest");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;
    //Option bru:type: 0=shutdown, 1 = reboot  
    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:PowerControlRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="1"/></bru:PowerControlRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }
  //End of coin return Part

  startReplenishment() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "StartReplenishmentFromEntranceOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;
    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:StartReplenishmentFromEntranceRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID></bru:StartReplenishmentFromEntranceRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

  endReplenishment() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "EndReplenishmentFromEntranceOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;
    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:EndReplenishmentFromEntranceRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID><Option bru:type="1"/></bru:EndReplenishmentFromEntranceRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }


  cancelReplenishment() {
    var headers = new Headers()
    headers.append("Accept", 'text/xml');
    headers.append('Content-Type', 'text/xml');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append("SOAPAction", "ReplenishmentFromEntranceCancelOperation");
    let options = new RequestOptions({ headers: headers });

    var link = this.machineIP;
    //Option bru:type: 0=shutdown, 1 = reboot  
    let requestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:bru="http://www.glory.co.jp/bruebox.xsd"><soapenv:Header/><soapenv:Body><bru:ReplenishmentFromEntranceCancelRequest><bru:Id>' + this.get_id + '</bru:Id><bru:SeqNo>' + this.get_seqNo + '</bru:SeqNo><bru:SessionID>' + this.get_sessionID + '</bru:SessionID></bru:ReplenishmentFromEntranceCancelRequest></soapenv:Body></soapenv:Envelope>'
    return new Promise(resolve => {
      this.http.post(link, requestString, options)
        .subscribe(data => {
          let status = data.status;
          if (status == 200) {
            var response = data['_body'];
            xml2js.parseString(response, function (err, result) {
              //console.log(result);
              resolve(result);
            });
          } else {
            console.log(status);
          }
        }, error => {
          console.log('error');
        });
    });
  }

}
