import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { AuthService } from '../../providers/auth-service/auth-service';
import { Insomnia } from '@ionic-native/insomnia';


@Injectable()
export class SessionProvider {

  public sessionTime;
  public expiredTime: Number;
  public sessionTimeSetting;
  constructor(private insomnia: Insomnia, private nativeStorage: NativeStorage, private auth: AuthService, public requestService: RequestServiceProvider) {
   
  }

  createSession() {
    let sessionTimeSetting;
    var currentTime = new Date();
    var hh = currentTime.getHours();
    var min = currentTime.getMinutes();
    var ss = currentTime.getSeconds();

    this.sessionTime = (hh * 60 * 60 * 1000) + (min * 60 * 1000) + (ss * 1000);

    this.nativeStorage.getItem('session_time').then(time => {
      this.sessionTimeSetting = time.time;
    })

    setTimeout(() => {
      let expiredSession = this.sessionTime + (Number(this.sessionTimeSetting) * 60 * 1000); //5 * 60 sec * 1000 ms
      window.localStorage.setItem('expired_session', expiredSession);
    }, 1000);

  }

  expiredSession() {
    let expiredSession = this.sessionTime + (Number(this.sessionTimeSetting) * 60 * 1000); //5 * 60 sec * 1000 ms
    window.localStorage.setItem('expired_session', expiredSession);
    return expiredSession;
  }

  sessionExpiredLogout() {
    this.requestService.requestMachineStatus().then(data => {
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].$["n:result"];
      if (statusCode == 0) {
        this.auth.logout().subscribe(succ => {
          window.localStorage.removeItem('expired_session');
          this.insomnia.allowSleepAgain();
          this.nativeStorage.remove('Login_Info');
        });
      }
      else {
        this.auth.logout().subscribe(succ => {
          window.localStorage.removeItem('expired_session');
          this.insomnia.allowSleepAgain();
          this.requestService.resetOperation();
          this.nativeStorage.remove('Login_Info');
        });
      }
    })
  }

}
