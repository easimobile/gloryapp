//to authenticate login

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { NativeStorage } from '@ionic-native/native-storage';

export class User {
  name: string;
  id: string;

  constructor(name: string, id: string) {
    this.name = name;
    this.id = id;
  }
}

@Injectable()
export class AuthService {
  currentUser: User;


  constructor(private nativeStorage: NativeStorage) { }

  public login(credentials) {
    if (credentials.id === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    }
    else {
      return Observable.create(observer => {
        this.nativeStorage.getItem('user_profile').then(data => {
          let access;
          let user_count = data.length;
          let get_user_name_native;
          let get_user_id_native;

          for (let i = 0; i < user_count; i++) {
            get_user_name_native = data[i].name;
            get_user_id_native = data[i].userid;
            let get_pass_native = data[i].pass;
            let get_user_status_native = data[i].status;
            let get_user_level_native = data[i].level;

            if (credentials.id === data[i].userid && credentials.password === data[i].pass && data[i].status === "1") {
              this.currentUser = new User(get_user_name_native, get_user_id_native);
              access = 1;
            }
            else if (credentials.id === data[i].userid && credentials.password === data[i].pass && data[i].status === "0") {
              access = 2;
            }
            else {
              access = 0;
            }
            
            if (access == 1)
              break;
          }
          
          observer.next(access);
          observer.complete();
        })
      });
    }
  }


  /*
  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }
  */

  public getUserInfo(): User {
    console.log(this.currentUser);
    return this.currentUser;
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }
}