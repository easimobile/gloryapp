import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { LoadingController, Loading } from 'ionic-angular';

@Injectable()
export class StatusCheckProvider {
  public loading;
  public loadingIsDisplay = 0;
  public interval;
  constructor(public loadingCtrl: LoadingController, public requestService: RequestServiceProvider) {

  }

  public checkStatusLoop() {
    clearInterval(this.interval);
    this.requestService.requestMachineStatus().then(data => {
      var dev_id_1 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus["0"].$["n:st"];   //cash machine
      var dev_id_2 = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"].DevStatus[1].$["n:st"];     //coin machine
      var statusCode = data["soapenv:Envelope"]["soapenv:Body"]["0"]["n:StatusResponse"]["0"].Status["0"]["n:Code"];

      if ((dev_id_1 == '1000' && dev_id_2 == '1000') || (dev_id_1 == '2000' && dev_id_2 == '2000') || (dev_id_1 == '2700' && dev_id_2 == '2700')) {
        this.dismissLoadingCustom();
      }
      //else if((dev_id_1 == '9400' || dev_id_2 == '9400' || dev_id_1 == '9200' || dev_id_2 == '9200')){
      else {
        console.log('Dev 1 Status Code: ' + dev_id_1);
        console.log('Dev 2 Status Code: ' + dev_id_2);
        console.log(window.localStorage.getItem('status'));
        if (window.localStorage.getItem('status') == null) {
          window.localStorage.setItem('status', '1');
          this.presentLoadingCustom();
        }
        this.loadingIsDisplay = 1;
        //this.presentLoadingCustom();
        if((dev_id_1 == '9400' || dev_id_2 == '9400' || dev_id_1 == '9200' || dev_id_2 == '9200')){
          this.interval = setInterval(() => this.requestService.resetOperation(), 5000);
        }

      }
    }
    )
  }

  public presentLoadingCustom() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    this.loading.present();
  }

  dismissLoadingCustom() {
    if (this.loadingIsDisplay == 1){
      this.loading.dismiss();
      this.loadingIsDisplay = 0;
      window.localStorage.removeItem('status');
    }
    
  }

}
